﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MegaDriveTo64 : MonoBehaviour {

    private const string ANIM_GUYFROMSEGATO64 = "GuyFromSegaTO64";
    private const string ANIM_GUYSEGATO64IDLE = "GuySegato64Idle";


    [SerializeField] UI.UIAnimator m_guy;

	bool m_sentEvent;

	// Use this for initialization
	void Start () {
        SNESEvents.OutOfScreen += SNESEvents_OutOfScreen;
	}

    private void SNESEvents_OutOfScreen()
    {
		if (m_sentEvent) {
			return;
		}

		m_sentEvent = true;
		SceneManager.LoadScene("F-Zero", LoadSceneMode.Additive);

        m_guy.gameObject.SetActive(true);
        Animator camAnimator = Camera.main.GetComponent<Animator>();
        camAnimator.enabled = true;
        Camera.main.GetComponent<UI.UIAnimator>().PlayAnimation("CamSegaTo64");
        m_guy.PlayAnimation(ANIM_GUYFROMSEGATO64,
            ()=>
            {
                SceneManager.UnloadSceneAsync("SNES");
                camAnimator.enabled = false;
				Camera.main.GetComponent<CameraController>().SetMegaDrive();
            });

        StartCoroutine(DelayedShit());
    }

    IEnumerator DelayedShit()
    {
        yield return new WaitForSeconds(0.4f);
        CWSoundManager.Instance.PlayEffect(GameSounds.Instance.IntoCOnsole);
    }

    
}
