 
#pragma strict
var isJumping = false;
var speed : float;
var maxSpeed : float;
var speedIncrease : float;
var speedDecrease : float;
var startSpeed : float;
var lastPress = 0;


var hitLeft = false;
var hitRight = false;


var jumping = false;
var playing = true;
var attacking = false;

var jumpForce : float;

var castDist : float;
var castDist2 : float;
var castDist3 : float;



//material 0 of the renderer
function Awake()
{
	//PlayerPrefs.DeleteAll();

	#if UNITY_IPHONE
	Application.targetFrameRate = 60;
	#endif


}

function Start () 
{

}

function Update () 
{

	

	//CASTING

	var CastUp1: RaycastHit2D = Physics2D.Raycast(Vector2(transform.position.x-0.1,transform.position.y+0.15),Vector2.up,castDist3);
   	var CastUp2: RaycastHit2D = Physics2D.Raycast(Vector2(transform.position.x,transform.position.y+0.15),Vector2.up,castDist3);
   	var CastUp3: RaycastHit2D = Physics2D.Raycast(Vector2(transform.position.x+0.1,transform.position.y+0.15),Vector2.up,castDist3);

   	if(CastUp1.collider!=null)
	{
		if(CastUp1.collider.tag=="Smash"&& this.GetComponent(Rigidbody).velocity.y>0)
		{
			
		}
	}
	else
	if(CastUp2.collider!=null)
	{
		if(CastUp2.collider.tag=="Smash"&& this.GetComponent(Rigidbody).velocity.y>0)
		{
			
		}
	}
	else
	if(CastUp3.collider!=null)
	{
		if(CastUp3.collider.tag=="Smash"&& this.GetComponent(Rigidbody).velocity.y>0)
		{
			
		}
	}










	var CastDown1: RaycastHit2D = Physics2D.Raycast(Vector2(transform.position.x-0.075,transform.position.y-0.15),-Vector2.up,castDist);
   	var CastDown2: RaycastHit2D = Physics2D.Raycast(Vector2(transform.position.x,transform.position.y-0.15),-Vector2.up,castDist);
   	var CastDown3: RaycastHit2D = Physics2D.Raycast(Vector2(transform.position.x+0.075,transform.position.y-0.15),-Vector2.up,castDist);
	
	Debug.DrawLine(Vector2(transform.position.x-0.1,transform.position.y-0.15),Vector2(transform.position.x-0.1,transform.position.y-0.2));
	Debug.DrawLine(Vector2(transform.position.x,transform.position.y-0.15),Vector2(transform.position.x,transform.position.y-0.2));
	Debug.DrawLine(Vector2(transform.position.x+0.1,transform.position.y-0.15),Vector2(transform.position.x+0.1,transform.position.y-0.2));

	//print("CASTDOWN1 = " + CastDown1.collider);
	//print("CASTDOWN2 = " + CastDown2.collider);
	//print("CASTDOWN3 = " + CastDown3.collider);

	if(CastDown1.collider!=null)
	{
		if((CastDown1.collider.tag=="Ground" || CastDown1.collider.tag=="Smash" || CastDown1.collider.tag=="Enemy") && this.GetComponent(Rigidbody).velocity.y<0)
		{
			
			jumping=false;
			attacking = false;


			
		}
	}
	else
	if(CastDown2.collider!=null)
	{
		if((CastDown2.collider.tag=="Ground"  || CastDown2.collider.tag=="Smash" || CastDown2.collider.tag=="Enemy") && this.GetComponent(Rigidbody).velocity.y<0)
		{
			
			jumping=false;
			attacking = false;

			
		}
	}
	else
	if(CastDown3.collider!=null)
	{
		if((CastDown3.collider.tag=="Ground"  || CastDown3.collider.tag=="Smash" || CastDown3.collider.tag=="Enemy") && this.GetComponent(Rigidbody).velocity.y<0)
		{
			
			jumping=false;
			attacking = false;


			
		}
	}
	


	var CastLeft1: RaycastHit2D = Physics2D.Raycast(Vector2(transform.position.x,transform.position.y+0.1),Vector2.left,castDist2);
   	var CastLeft2: RaycastHit2D = Physics2D.Raycast(Vector2(transform.position.x,transform.position.y),Vector2.left,castDist2);
   	var CastLeft3: RaycastHit2D = Physics2D.Raycast(Vector2(transform.position.x,transform.position.y-0.1),Vector2.left,castDist2);

   	//print("CASTDOWN1 = " + CastLeft1.collider);
	//print("CASTDOWN2 = " + CastLeft2.collider);
	//print("CASTDOWN3 = " + CastLeft3.collider);

	if(CastLeft1.collider!=null)
	{
		if(CastLeft1.collider.tag=="Ground" || CastLeft1.collider.tag=="Smash")
		{
			hitLeft=true;
		}
		else
		{
			hitLeft=false;
		}
	}
	else
	if(CastLeft2.collider!=null)
	{
		if(CastLeft2.collider.tag=="Ground" || CastLeft2.collider.tag=="Smash")
		{
			hitLeft=true;
		}
		else
		{
			hitLeft=false;
		}
	}
	else
	if(CastLeft3.collider!=null)
	{
		if(CastLeft3.collider.tag=="Ground" || CastLeft3.collider.tag=="Smash")
		{
			hitLeft=true;
		}
		else
		{
			hitLeft=false;
		}
	}
	else
	{
		hitLeft=false;
	}



	var CastRight1: RaycastHit2D = Physics2D.Raycast(Vector2(transform.position.x,transform.position.y+0.1),-Vector2.left,castDist2);
   	var CastRight2: RaycastHit2D = Physics2D.Raycast(Vector2(transform.position.x,transform.position.y),-Vector2.left,castDist2);
   	var CastRight3: RaycastHit2D = Physics2D.Raycast(Vector2(transform.position.x,transform.position.y-0.1),-Vector2.left,castDist2);

   	//print("CASTDOWN1 = " + CastRight1.collider);
	//print("CASTDOWN2 = " + CastRight2.collider);
	//print("CASTDOWN3 = " + CastRight3.collider);

	if(CastRight1.collider!=null)
	{
		if(CastRight1.collider.tag=="Ground"  || CastRight1.collider.tag=="Smash")
		{
			hitRight=true;
		}
		else
		{
			hitRight=false;
		}
	}
	else
	if(CastRight2.collider!=null)
	{
		if(CastRight2.collider.tag=="Ground"  || CastRight2.collider.tag=="Smash")
		{
			hitRight=true;
		}
		else
		{
			hitRight=false;
		}
	}
	else
	if(CastRight3.collider!=null)
	{
		if(CastRight3.collider.tag=="Ground"  || CastRight3.collider.tag=="Smash")
		{
			hitRight=true;
		}
		else
		{
			hitRight=false;
		}
	}
	else
	{
		hitRight=false;
	}



	if(Input.GetMouseButtonDown(0) && playing && !jumping && !attacking)
	{
		jumping = true;
		Jump();
	}
	else
	if(Input.GetMouseButtonDown(0) && playing && jumping && !attacking)
	{
		attacking = true;
		Attack();
	}

	if (Input.GetKey ("right") && playing)
	{
		this.transform.rotation.y = 0;
		lastPress = 0;

		if(!jumping)
		{
			this.GetComponent(Animator).Play("Player_run");
		}

		if(!hitRight)
		{
			if(speed<startSpeed)
			{
				speed = startSpeed;
			}
			transform.position.x+=(speed*Time.deltaTime);
			if(speed<maxSpeed)
			{
				speed+=(speedIncrease*Time.deltaTime);
			}
		}
		else
		{
			speed=0;
		}

	}
	else
	if (Input.GetKey ("left") && playing)
	{
		
		this.transform.rotation.y = 180;
		lastPress = 1;

		if(!jumping)
		{
			this.GetComponent(Animator).Play("Player_run");
		}

		if(!hitLeft)
		{
			if(speed<startSpeed)
			{
				speed = startSpeed;
			}

			transform.position.x-=(speed*Time.deltaTime);
			if(speed<maxSpeed)
			{
				speed+=(speedIncrease *Time.deltaTime);
			}
		}
		else
		{
			speed=0;
		}

	}
	else
	{
		if(!jumping && playing)
		{
			this.GetComponent(Animator).Play("Player_idle");
		}

		if(speed>0)
		{
			speed-=speedDecrease;
			if(speed<0)
			speed=0;

		}

		if(lastPress == 0)
		{
			transform.position.x+=(speed *Time.deltaTime);
		}
		else
		{
			transform.position.x-=(speed *Time.deltaTime);
		}
	}




}

//function OnTriggerEnter2D(other: Collider2D) 
//{
//
//	if(other.tag=="Wall")
//	{
//		hitLeft=true;
//	}
//
//}
//
//function OnTriggerExit2D(other: Collider2D) 
//{
//
//	if(other.tag=="Wall")
//	{
//		hitLeft=false;
//	}
//
//}

function OnCollisionEnter2D(coll: Collision2D) 
{
    
   
}

function Jump()
{

	this.GetComponent(Rigidbody).AddForce(Vector2(0,jumpForce));
	print("NUMBER 7");
	this.GetComponent(Animator).Play("Player_jump_up");


}

function Attack()
{

	this.GetComponent(Rigidbody).velocity.y = 0;
	this.GetComponent(Rigidbody).AddForce(Vector2(0,-jumpForce));
	this.GetComponent(Animator).Play("Player_spin_attack");

}





//iTween.MoveTo(gameObject, iTween.Hash("position", Vector3(0,0,0), "time", 1.5, "easetype", iTween.EaseType.easeInOutSine));

