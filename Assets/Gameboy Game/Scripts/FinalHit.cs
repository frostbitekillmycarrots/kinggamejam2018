﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinalHit : MonoBehaviour 
{
	[SerializeField] GameObject m_3dPlayerPrefab;
	[SerializeField] GameObject m_spawnpoint;
	public GameObject player;

	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	void OnTriggerEnter2D(Collider2D col)
	{
		if (col.tag == "Player") 
		{
			GameObject env = GameObject.Find ("Environment");

			GameObject obj = Instantiate (m_3dPlayerPrefab, env.transform);
			obj.transform.position = m_spawnpoint.transform.position;

			player.active = false;
		}
	}
}
