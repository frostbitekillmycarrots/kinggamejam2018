 
#pragma strict
var splash : GameObject;
var splashMoveTo : Vector3;
var splashSpeed : float;
var mainMenu : GameObject;
var gamePlay : GameObject;

var menuLogo : GameObject;
var logoSpeed : float;
var menuShip : GameObject;
var menuPlayer : GameObject;
var menuPod : GameObject;

var readyToStart = false;

var gamePod : GameObject;
var gamePlayer : GameObject;

var debug = true;

var background : GameObject;

var started = false;

var boomSFX : GameObject;

function Awake()
{
	//PlayerPrefs.DeleteAll();

	#if UNITY_IPHONE
	Application.targetFrameRate = 60;
	#endif

}

function Start () 
{

}

function Update () 
{
	if(transform.localScale.x>1 && !started)
	{
		started = true;
		Splash();
	}

	if(Input.GetMouseButtonDown(0) && readyToStart)
	{
		readyToStart = false;
		Intro();
	}
}

function OnTriggerEnter2D(other: Collider2D) 
{



}

function OnCollisionEnter2D(coll: Collision2D) 
{
    


}

function Splash()
{

	while(splash.transform.position.y>splashMoveTo.y)
	{
		splash.transform.position.y-=(splashSpeed*Time.deltaTime);
		yield WaitForSeconds(0.1);
	}

	splash.GetComponent(AudioSource).Play();
	yield WaitForSeconds(2);

	while(splash.GetComponent(SpriteRenderer).color.a>0)
	{
		splash.GetComponent(SpriteRenderer).color.a-=(8*Time.deltaTime);
		yield WaitForSeconds(0.1);
	}

	yield WaitForSeconds(0.5);

	mainMenu.active=true;

	while(menuShip.GetComponent(SpriteRenderer).color.a<1)
	{
		menuShip.GetComponent(SpriteRenderer).color.a+=0.2;
		menuPlayer.GetComponent(SpriteRenderer).color.a+=0.2;
		yield WaitForSeconds(0.1);
	}



	while(menuLogo.transform.position.x<0)
	{
		menuLogo.transform.position.x+=logoSpeed*Time.deltaTime;
		yield WaitForSeconds(0.05);
	}

	readyToStart=true;
}

function Intro()
{

	menuPlayer.active=false;
	menuPod.active=true;
	yield WaitForSeconds(1.5);

	mainMenu.active=false;
	gamePlay.active=true;
	background.active=true;

	yield WaitForSeconds(1);
	gamePod.active=true;
	yield WaitForSeconds(0.3);
	gamePlayer.active=true;
	boomSFX.active=true;

}





//iTween.MoveTo(gameObject, iTween.Hash("position", Vector3(0,0,0), "time", 1.5, "easetype", iTween.EaseType.easeInOutSine));

