﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameBoyPlayer3DWorldC : MonoBehaviour
{

	private const string ANIM_GAMEBOY3DGUYIDLE = "Gameboy3DGuyIdle";
	private const string ANIM_GAMEBOY3DGUYOUT = "GameBoy3DGuyOut";
	private const string ANIM_GAMEBOY3DGUYINSEGA = "GameBoy3DGuyInSega";


	private const string ANIM_CAMINTRO = "CamIntro";
	private const string ANIM_NONE = "None";
	private const string ANIM_CAMGAMEBOYTOSEGA = "CamGameBoyToSega";
	private const string ANIM_CAMSEGATOTV = "CamSegaToTv";



	[SerializeField] UI.UIAnimator m_uiANimator;
	[SerializeField] Animator m_ANimator2;
	[SerializeField] SpriteRenderer m_sprite;
	bool m_controllable;

	Animator m_cameraAnimator;
	UI.UIAnimator m_cameraUIAnimator;
	CameraController m_cam;

	// Use this for initialization
	void Start ()
	{
		m_cameraAnimator = Camera.main.GetComponent<Animator> ();
		m_cameraUIAnimator = Camera.main.GetComponent<UI.UIAnimator> ();
		m_cam = Camera.main.GetComponent<CameraController> ();

		m_uiANimator.PlayAnimation (ANIM_GAMEBOY3DGUYOUT, () => {
			m_controllable = true;
			m_cameraAnimator.enabled = true;
			m_cameraUIAnimator.PlayAnimation (ANIM_CAMGAMEBOYTOSEGA, () => {
				m_cameraAnimator.enabled = false;
				m_cam.SetFollowPlayer (this);
               
                SceneManager.UnloadSceneAsync ("Gameboy_Game");
			});
			m_uiANimator.PlayAnimation (ANIM_GAMEBOY3DGUYIDLE);


			GameObject env = GameObject.Find ("GameBoyGuyContainer");

			transform.SetParent (env.transform);

			transform.localPosition = Vector3.zero;
			transform.localRotation = Quaternion.identity;
            m_ANimator2.Play("Player_idle");
        });

        m_ANimator2.Play("Player_jump_up");
        CWSoundManager.Instance.PlayEffect(GameSounds.Instance.Jump);
    }

    float offset = 0;
    bool m_jump;
    float m_jumpSpeed = 0.5f;
	// Update is called once per frame
	void Update ()
	{
        if (m_controllable)
        {
            if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D))
            {
                if (!m_jump)
                    m_ANimator2.Play("Player_run");

                float setp = 0.1f * Time.deltaTime;
                offset += setp;
                transform.position += setp * transform.right;
                m_sprite.flipX = false;
            }
            if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A))
            {
                if (!m_jump)
                    m_ANimator2.Play("Player_run");

                m_sprite.flipX = true;

                float setp = 0.1f  * Time.deltaTime;

                if (offset - setp > -0.05f)
                {
                    offset -= setp;
                    transform.position -= transform.right * setp;
                }
               

            }

            if (Input.GetKeyUp(KeyCode.RightArrow))
            {
                m_ANimator2.Play("Player_idle");
            }

            if (Input.GetKeyUp(KeyCode.LeftArrow))
            {
                m_ANimator2.Play("Player_idle");
            }

            if (Input.GetMouseButton(0) && !m_jump)
            {
                m_ANimator2.Play("Player_jump_up");
                m_jump = true;
                m_jumpSpeed = 0.5f;
                CWSoundManager.Instance.PlayEffect(GameSounds.Instance.Jump);
            }

            if (m_jump)
            {
                transform.position += m_jumpSpeed * transform.up * Time.deltaTime;
                m_jumpSpeed -= Time.deltaTime * 2f;
            }
        }
	}

    private void OnCollisionEnter(Collision collision)
    {
        if (m_jump)
        {
            m_ANimator2.Play("Player_idle");
            m_jump = false;
            m_jumpSpeed = 0;
        }
    }

    void OnTriggerEnter (Collider other)
	{
		m_controllable = false;
		m_ANimator2.Play("Player_idle");
        StartCoroutine(PlayDelayed());
        m_uiANimator.PlayAnimation (ANIM_GAMEBOY3DGUYINSEGA,
			() => {
				SceneManager.LoadScene ("SNES", LoadSceneMode.Additive);
				m_cameraAnimator.enabled = true;
				m_cameraUIAnimator.PlayAnimation (ANIM_CAMSEGATOTV, () => {
					m_cam.SetMegaDrive();
					m_cameraAnimator.enabled = false;
				});
			});
	}

    IEnumerator PlayDelayed()
    {
        yield return new WaitForSeconds(0.5f);
        CWSoundManager.Instance.PlayEffect(GameSounds.Instance.IntoCOnsole);
    }

}
