 
#pragma strict
var isJumping = false;
var speed : float;
var maxSpeed : float;
var speedIncrease : float;
var speedDecrease : float;
var startSpeed : float;
var lastPress = 0;

var camFudge : float;
var hitLeft = false;
var hitRight = false;

var midGroundFudge : float;
var midGround : GameObject;
var midGroundStart : Vector3;

var jumping = false;
var playing = true;
var attacking = false;

var jumpForce : float;

var castDist : float;
var castDist2 : float;
var castDist3 : float;

var blockSmash : GameObject;

var endFlow = 0;

var myCam : GameObject;

var checkpoint : GameObject[];

var finalHit : GameObject;

var crackTexture : Texture[];

var coinSFX : GameObject;
var jumpSFX : GameObject;
var BGM : GameObject;

var glassCrackSFX : GameObject;
var glassSmashSFX : GameObject;

var leftWall : GameObject;


//material 0 of the renderer
function Awake()
{
	//PlayerPrefs.DeleteAll();

	#if UNITY_IPHONE
	Application.targetFrameRate = 60;
	#endif

	midGroundStart = midGround.transform.position;

}

function Start () 
{

}

function Update () 
{


	if(this.transform.position.y<-0.8 && playing)
	{
		Dead();
	}

	//CASTING

	var CastUp1: RaycastHit2D = Physics2D.Raycast(Vector2(transform.position.x-0.1,transform.position.y+0.15),Vector2.up,castDist3);
   	var CastUp2: RaycastHit2D = Physics2D.Raycast(Vector2(transform.position.x,transform.position.y+0.15),Vector2.up,castDist3);
   	var CastUp3: RaycastHit2D = Physics2D.Raycast(Vector2(transform.position.x+0.1,transform.position.y+0.15),Vector2.up,castDist3);

   	if(CastUp1.collider!=null)
	{
		if(CastUp1.collider.tag=="Smash"&& this.GetComponent(Rigidbody2D).velocity.y>0)
		{
			if(CastUp1.collider.tag=="Smash")
			{

				if(CastUp1.collider.gameObject.GetComponent(Block).endBlock>0)
				{
					endFlow = CastUp1.collider.gameObject.GetComponent(Block).endBlock;
				}

				print("SMASH1");
				Instantiate(blockSmash,CastUp1.collider.gameObject.transform.position,transform.rotation);
				CastUp1.collider.gameObject.GetComponent(Block).markForDeath = true;
				this.GetComponent(Rigidbody2D).velocity.y = 0;
				attacking = false;
				print("NUMBER 11");
				this.GetComponent(Animator).Play("Player_jump_up");
			}
		}
	}
	else
	if(CastUp2.collider!=null)
	{
		if(CastUp2.collider.tag=="Smash"&& this.GetComponent(Rigidbody2D).velocity.y>0)
		{
			if(CastUp2.collider.tag=="Smash")
			{
				if(CastUp2.collider.gameObject.GetComponent(Block).endBlock>0)
				{
					endFlow = CastUp2.collider.gameObject.GetComponent(Block).endBlock;
				}

				print("SMASH1");
				Instantiate(blockSmash,CastUp2.collider.gameObject.transform.position,transform.rotation);
				CastUp2.collider.gameObject.GetComponent(Block).markForDeath = true;
				this.GetComponent(Rigidbody2D).velocity.y = 0;
				attacking = false;
				print("NUMBER 10");
				this.GetComponent(Animator).Play("Player_jump_up");
			}
		}
	}
	else
	if(CastUp3.collider!=null)
	{
		if(CastUp3.collider.tag=="Smash"&& this.GetComponent(Rigidbody2D).velocity.y>0)
		{
			if(CastUp3.collider.tag=="Smash")
			{

				if(CastUp3.collider.gameObject.GetComponent(Block).endBlock>0)
				{
					endFlow = CastUp3.collider.gameObject.GetComponent(Block).endBlock;
				}

				print("SMASH1");
				Instantiate(blockSmash,CastUp3.collider.gameObject.transform.position,transform.rotation);
				CastUp3.collider.gameObject.GetComponent(Block).markForDeath = true;
				this.GetComponent(Rigidbody2D).velocity.y = 0;
				attacking = false;
				print("NUMBER 1");
				this.GetComponent(Animator).Play("Player_jump_up");
			}
		}
	}










	var CastDown1: RaycastHit2D = Physics2D.Raycast(Vector2(transform.position.x-0.075,transform.position.y-0.15),-Vector2.up,castDist);
   	var CastDown2: RaycastHit2D = Physics2D.Raycast(Vector2(transform.position.x,transform.position.y-0.15),-Vector2.up,castDist);
   	var CastDown3: RaycastHit2D = Physics2D.Raycast(Vector2(transform.position.x+0.075,transform.position.y-0.15),-Vector2.up,castDist);
	
	Debug.DrawLine(Vector2(transform.position.x-0.1,transform.position.y-0.15),Vector2(transform.position.x-0.1,transform.position.y-0.2));
	Debug.DrawLine(Vector2(transform.position.x,transform.position.y-0.15),Vector2(transform.position.x,transform.position.y-0.2));
	Debug.DrawLine(Vector2(transform.position.x+0.1,transform.position.y-0.15),Vector2(transform.position.x+0.1,transform.position.y-0.2));

	//print("CASTDOWN1 = " + CastDown1.collider);
	//print("CASTDOWN2 = " + CastDown2.collider);
	//print("CASTDOWN3 = " + CastDown3.collider);

	if(CastDown1.collider!=null)
	{
		if((CastDown1.collider.tag=="Ground" || CastDown1.collider.tag=="Smash" || CastDown1.collider.tag=="Enemy") && this.GetComponent(Rigidbody2D).velocity.y<0)
		{
			if(CastDown1.collider.tag=="Smash" && attacking)
			{
				print("SMASH1");
				Instantiate(blockSmash,CastDown1.collider.gameObject.transform.position,transform.rotation);
				CastDown1.collider.gameObject.GetComponent(Block).markForDeath = true;
				this.GetComponent(Rigidbody2D).velocity.y = 0;
				this.GetComponent(Rigidbody2D).AddForce(Vector2(0,20));
				attacking = false;
				print("NUMBER 2");
				this.GetComponent(Animator).Play("Player_jump_up");
			}
			else
			if(CastDown1.collider.tag=="Enemy" && attacking)
			{
				CastDown1.collider.gameObject.GetComponent(Enemy).markForDeath = true;
				this.GetComponent(Rigidbody2D).velocity.y = 0;
				this.GetComponent(Rigidbody2D).AddForce(Vector2(0,20));
				attacking = false;
				print("NUMBER 3");
				this.GetComponent(Animator).Play("Player_jump_up");
			}
			else
			{
				

				jumping=false;
				attacking = false;

				if(endFlow>0)
				{
					EndFlow();	
				}
			}
		}
	}
	else
	if(CastDown2.collider!=null)
	{
		if((CastDown2.collider.tag=="Ground"  || CastDown2.collider.tag=="Smash" || CastDown2.collider.tag=="Enemy") && this.GetComponent(Rigidbody2D).velocity.y<0)
		{
			if(CastDown2.collider.tag=="Smash" && attacking)
			{
				print("SMASH2");
				Instantiate(blockSmash,CastDown2.collider.gameObject.transform.position,transform.rotation);
				CastDown2.collider.gameObject.GetComponent(Block).markForDeath = true;
				this.GetComponent(Rigidbody2D).velocity.y = 0;
				this.GetComponent(Rigidbody2D).AddForce(Vector2(0,20));
				attacking = false;
				print("NUMBER 4");
				this.GetComponent(Animator).Play("Player_jump_up");
			}
			else
			if(CastDown2.collider.tag=="Enemy" && attacking)
			{
				CastDown2.collider.gameObject.GetComponent(Enemy).markForDeath = true;
				this.GetComponent(Rigidbody2D).velocity.y = 0;
				this.GetComponent(Rigidbody2D).AddForce(Vector2(0,20));
				attacking = false;
				print("NUMBER 5");
				this.GetComponent(Animator).Play("Player_jump_up");
			}
			else
			{
				jumping=false;
				attacking = false;

				if(endFlow>0)
				{
					EndFlow();	
				}
			}
		}
	}
	else
	if(CastDown3.collider!=null)
	{
		if((CastDown3.collider.tag=="Ground"  || CastDown3.collider.tag=="Smash" || CastDown3.collider.tag=="Enemy") && this.GetComponent(Rigidbody2D).velocity.y<0)
		{
			if(CastDown3.collider.tag=="Smash" && attacking)
			{
				print("SMASH3");
				Instantiate(blockSmash,CastDown3.collider.gameObject.transform.position,transform.rotation);
				CastDown3.collider.gameObject.GetComponent(Block).markForDeath = true;
				this.GetComponent(Rigidbody2D).velocity.y = 0;
				this.GetComponent(Rigidbody2D).AddForce(Vector2(0,20));
				attacking = false;
				print("NUMBER 6");
				this.GetComponent(Animator).Play("Player_jump_up");
			}
			else
			if(CastDown3.collider.tag=="Enemy" && attacking)
			{
				CastDown3.collider.gameObject.GetComponent(Enemy).markForDeath = true;
				this.GetComponent(Rigidbody2D).velocity.y = 0;
				this.GetComponent(Rigidbody2D).AddForce(Vector2(0,20));
				attacking = false;
				print("NUMBER 8");
				this.GetComponent(Animator).Play("Player_jump_up");
			}
			else
			{
				jumping=false;
				attacking = false;

				if(endFlow>0)
				{
					EndFlow();	
				}
			}
		}
	}
	


	var CastLeft1: RaycastHit2D = Physics2D.Raycast(Vector2(transform.position.x,transform.position.y+0.1),Vector2.left,castDist2);
   	var CastLeft2: RaycastHit2D = Physics2D.Raycast(Vector2(transform.position.x,transform.position.y),Vector2.left,castDist2);
   	var CastLeft3: RaycastHit2D = Physics2D.Raycast(Vector2(transform.position.x,transform.position.y-0.1),Vector2.left,castDist2);

   	//print("CASTDOWN1 = " + CastLeft1.collider);
	//print("CASTDOWN2 = " + CastLeft2.collider);
	//print("CASTDOWN3 = " + CastLeft3.collider);

	if(CastLeft1.collider!=null)
	{
		if(CastLeft1.collider.tag=="Ground" || CastLeft1.collider.tag=="Smash")
		{
			hitLeft=true;
		}
		else
		{
			hitLeft=false;
		}
	}
	else
	if(CastLeft2.collider!=null)
	{
		if(CastLeft2.collider.tag=="Ground" || CastLeft2.collider.tag=="Smash")
		{
			hitLeft=true;
		}
		else
		{
			hitLeft=false;
		}
	}
	else
	if(CastLeft3.collider!=null)
	{
		if(CastLeft3.collider.tag=="Ground" || CastLeft3.collider.tag=="Smash")
		{
			hitLeft=true;
		}
		else
		{
			hitLeft=false;
		}
	}
	else
	{
		hitLeft=false;
	}



	var CastRight1: RaycastHit2D = Physics2D.Raycast(Vector2(transform.position.x,transform.position.y+0.1),-Vector2.left,castDist2);
   	var CastRight2: RaycastHit2D = Physics2D.Raycast(Vector2(transform.position.x,transform.position.y),-Vector2.left,castDist2);
   	var CastRight3: RaycastHit2D = Physics2D.Raycast(Vector2(transform.position.x,transform.position.y-0.1),-Vector2.left,castDist2);

   	//print("CASTDOWN1 = " + CastRight1.collider);
	//print("CASTDOWN2 = " + CastRight2.collider);
	//print("CASTDOWN3 = " + CastRight3.collider);

	if(CastRight1.collider!=null)
	{
		if(CastRight1.collider.tag=="Ground"  || CastRight1.collider.tag=="Smash")
		{
			hitRight=true;
		}
		else
		{
			hitRight=false;
		}
	}
	else
	if(CastRight2.collider!=null)
	{
		if(CastRight2.collider.tag=="Ground"  || CastRight2.collider.tag=="Smash")
		{
			hitRight=true;
		}
		else
		{
			hitRight=false;
		}
	}
	else
	if(CastRight3.collider!=null)
	{
		if(CastRight3.collider.tag=="Ground"  || CastRight3.collider.tag=="Smash")
		{
			hitRight=true;
		}
		else
		{
			hitRight=false;
		}
	}
	else
	{
		hitRight=false;
	}



	if(Input.GetMouseButtonDown(0) && playing && !jumping && !attacking)
	{
		jumping = true;
		Jump();
	}
	else
	if(Input.GetMouseButtonDown(0) && playing && jumping && !attacking)
	{
		attacking = true;
		Attack();
	}

	if ((Input.GetKey ("right") || Input.GetKey ("d")) && playing)
	{
		this.transform.rotation.y = 0;
		lastPress = 0;

		if(!jumping)
		{
			this.GetComponent(Animator).Play("Player_run");
		}

		if(!hitRight)
		{
			if(speed<startSpeed)
			{
				speed = startSpeed;
			}
			transform.position.x+=(speed*Time.deltaTime);
			if(speed<maxSpeed)
			{
				speed+=(speedIncrease*Time.deltaTime);
			}
		}
		else
		{
			speed=0;
		}

	}
	else
	if ((Input.GetKey ("left") || Input.GetKey ("a")) && playing)
	{
		
		this.transform.rotation.y = 180;
		lastPress = 1;

		if(!jumping)
		{
			this.GetComponent(Animator).Play("Player_run");
		}

		if(!hitLeft)
		{
			if(speed<startSpeed)
			{
				speed = startSpeed;
			}

			transform.position.x-=(speed*Time.deltaTime);
			if(speed<maxSpeed)
			{
				speed+=(speedIncrease *Time.deltaTime);
			}
		}
		else
		{
			speed=0;
		}

	}
	else
	{
		if(!jumping && playing)
		{
			this.GetComponent(Animator).Play("Player_idle");
		}

		if(speed>0)
		{
			speed-=speedDecrease;
			if(speed<0)
			speed=0;

		}

		if(lastPress == 0)
		{
			transform.position.x+=(speed *Time.deltaTime);
		}
		else
		{
			transform.position.x-=(speed *Time.deltaTime);
		}
	}



	if(transform.position.x< 16 && transform.position.x>(myCam.transform.position.x-camFudge))
	{
		var diff = midGroundStart.x - myCam.transform.position.x;

		midGround.transform.position.x= midGroundStart.x-(diff*midGroundFudge);

		myCam.transform.position.x = transform.position.x+camFudge;
	}
}

//function OnTriggerEnter2D(other: Collider2D) 
//{
//
//	if(other.tag=="Wall")
//	{
//		hitLeft=true;
//	}
//
//}
//
//function OnTriggerExit2D(other: Collider2D) 
//{
//
//	if(other.tag=="Wall")
//	{
//		hitLeft=false;
//	}
//
//}

function OnCollisionEnter2D(coll: Collision2D) 
{
    
    if(coll.gameObject.tag == "Enemy" && playing)
    {
    	print("HIT ENEMY");
    	if(!attacking)
    	{
    		playing=false;
    		this.GetComponent(Collider2D).enabled=false;
    		//this.GetComponent(Rigidbody2D).simulated=false;
    		this.transform.rotation.x = -180;
			iTween.MoveTo(this.gameObject, iTween.Hash("position", Vector3(transform.position.x,transform.position.y+0.1,transform.position.z), "time", 0.5, "easetype", iTween.EaseType.easeInOutSine));
			yield WaitForSeconds(0.5);
			iTween.MoveTo(this.gameObject, iTween.Hash("position", Vector3(transform.position.x,transform.position.y-3,transform.position.z), "time", 0.9, "easetype", iTween.EaseType.easeInOutSine));
			yield WaitForSeconds(1);
			this.GetComponent(Collider2D).enabled=true;
    		this.transform.rotation.x = 0;
    		Dead();
    	}
    }
    else
    {

    	if(coll.gameObject.tag == "Coin" && playing)
    	{

    		Instantiate(coinSFX,transform.position,transform.rotation);
    		Destroy(coll.gameObject);

    	}

    }


    if(coll.gameObject.tag == "Enemy" || coll.gameObject.tag == "Ground"  || coll.gameObject.tag == "Smash")
    {

    	jumping=false;
    	attacking=false;

    }

}

function Jump()
{

	Instantiate(jumpSFX,transform.position,transform.rotation);
	this.GetComponent(Rigidbody2D).AddForce(Vector2(0,jumpForce));
	print("NUMBER 7");
	this.GetComponent(Animator).Play("Player_jump_up");


}

function Attack()
{

	this.GetComponent(Rigidbody2D).velocity.y = 0;
	this.GetComponent(Rigidbody2D).AddForce(Vector2(0,-jumpForce));
	this.GetComponent(Animator).Play("Player_spin_attack");

}

function EndFlow()
{

	leftWall.active=false;
	if(endFlow==1)
	{
		print("END FLOW");
		BGM.active=false;

		endFlow=0;
		playing = false;
		glassCrackSFX.active=true;
		lastPress = 1;
		speed = 3.5;
		this.GetComponent(Animator).Play("Player_fall");

		GameObject.Find("Crack").GetComponent(Renderer).material.mainTexture = crackTexture[0];
		yield WaitForSeconds(2);
		this.GetComponent(Animator).Play("Player_stand");
		yield WaitForSeconds(0.5);
		this.GetComponent(Animator).Play("Player_idle");
		playing=true;

	}
	else
	if(endFlow==2)
	{
		print("END FLOW");

		endFlow=0;
		playing = false;
		glassSmashSFX.active=true;
		lastPress = 1;
		speed = 3.5;
		this.GetComponent(Animator).Play("Player_fall");

		GameObject.Find("Crack").GetComponent(Renderer).material.mainTexture = crackTexture[1];

		yield WaitForSeconds(2);
		this.GetComponent(Animator).Play("Player_stand");
		yield WaitForSeconds(0.5);
		this.GetComponent(Animator).Play("Player_idle");
		playing=true;
		finalHit.active=true;
	}

}

function Dead()
{

	playing = false;
	if(this.transform.position.x<10)
	{
		this.transform.position = checkpoint[0].transform.position;
		myCam.transform.position.x = this.transform.position.x;
		this.GetComponent(Rigidbody2D).velocity = Vector2(0,0);
	}
	else
	if(this.transform.position.x<15)
	{
		this.transform.position = checkpoint[1].transform.position;
		myCam.transform.position.x = this.transform.position.x;
		this.GetComponent(Rigidbody2D).velocity = Vector2(0,0);
	}
	else
	{
		this.transform.position = checkpoint[2].transform.position;
		myCam.transform.position.x = this.transform.position.x;
		this.GetComponent(Rigidbody2D).velocity = Vector2(0,0);
	}

	for(var x = 0; x < 5; x++)
	{

		this.GetComponent(SpriteRenderer).enabled=false;
		yield WaitForSeconds(0.1);
		this.GetComponent(SpriteRenderer).enabled=true;
		yield WaitForSeconds(0.1);

	}
	playing = true;


}



//iTween.MoveTo(gameObject, iTween.Hash("position", Vector3(0,0,0), "time", 1.5, "easetype", iTween.EaseType.easeInOutSine));

