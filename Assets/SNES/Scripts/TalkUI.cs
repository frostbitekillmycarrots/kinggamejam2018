﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TalkUI : MonoBehaviour {

	// Use this for initialization
	void Awake () {

        gameObject.SetActive(false);
        SNESEvents.ConversationEntered += SNESEvents_ConversationEntered;
        SNESEvents.ConversationExit += SNESEvents_ConversationExit;
        SNESEvents.ConversationStarted += SNESEvents_ConversationStarted;
    }

    private void SNESEvents_ConversationStarted(Conversation c)
    {
        gameObject.SetActive(false);
    }

    private void OnDestroy()
    {

        SNESEvents.ConversationEntered -= SNESEvents_ConversationEntered;
        SNESEvents.ConversationExit -= SNESEvents_ConversationExit;
        SNESEvents.ConversationStarted -= SNESEvents_ConversationStarted;
    }

    private void SNESEvents_ConversationEntered(Conversation c)
    {
        gameObject.SetActive(true);
    }
    private void SNESEvents_ConversationExit(Conversation c)
    {
        gameObject.SetActive(false);
    }
}
