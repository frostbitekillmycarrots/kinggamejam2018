﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Props : MonoBehaviour {

	// Use this for initialization
	void Start () {
        foreach (Transform child in transform)
        {
            Vector3 position = child.position;
            position.z = position.y;
            child.position = position;
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
