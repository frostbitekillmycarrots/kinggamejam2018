﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Conversation : MonoBehaviour {


    public enum CONDITIONS
    {
        NONE,
        NPC1_STATE_0,
        NPC1_STATE_1,
            NPC1_STATE_2,
        NPC2_STATE_0,
        NPC2_STATE_1,
        NPC2_STATE_2,
        NPC3_STATE_0,
        NPC3_STATE_1,
        NPC4_STATE_0,
        NPC4_STATE_1,
        NPC4_STATE_2,
        DOOR,
        NPC0_STATE_0,
        NPC0_STATE_1
    }

    public enum REWARDS
    {
        NONE,
        PISTOL,
        BULLETS,
        KEY,
        TALKWITHKID,
        FATHER
    }

    public void GiveReward()
    {
        switch (reward)
        {
            case REWARDS.BULLETS:
                {
                    Inventory.HAS_BULLETS = true;

                    CWSoundManager.Instance.PlayEffect(GameSounds.Instance.Progress);
                }
                break;
            case REWARDS.KEY:
                {
                    CWSoundManager.Instance.PlayEffect(GameSounds.Instance.Progress);
                    Inventory.HAS_THE_KEY = true;
                    GameObject go = GameObject.Find("NPC3Girl");
                    go.transform.localPosition = new Vector3(-1.06f, -0.89f, go.transform.localPosition.z);
                    go = GameObject.Find("Father");
                    go.transform.localPosition = new Vector3(-2.95f, -3f, go.transform.localPosition.z);
                }
                break;
            case REWARDS.PISTOL:
                {
                    CWSoundManager.Instance.PlayEffect(GameSounds.Instance.Progress);
                    Inventory.HAS_THE_GUN = true;
                }
                break;
            case REWARDS.TALKWITHKID:
                {
                    CWSoundManager.Instance.PlayEffect(GameSounds.Instance.Progress);
                    Inventory.TALKED_WITH_X = true;
                }
                break;
            case REWARDS.FATHER:
                {
                    GameObject.Find("fence").GetComponent<UI.UIAnimator>().PlayAnimation("OpenFence",
                        ()=>
                        {
                            SNESEvents.SendFatherEvent();
                        });
                }
                break;

        }
    }
    public bool DoesMeetCOndition()
    {
        switch (condition)
        {
            case CONDITIONS.NPC1_STATE_0:
                {
                    return !Inventory.TALKED_WITH_X;
                }
            case CONDITIONS.NPC1_STATE_1:
                {
                    return Inventory.TALKED_WITH_X && !Inventory.HAS_THE_GUN;
                }
                
            case CONDITIONS.NPC1_STATE_2:
                {
                    return Inventory.TALKED_WITH_X && Inventory.HAS_THE_GUN;
                }
            case CONDITIONS.NPC2_STATE_0:
                {
                    return !Inventory.HAS_THE_GUN;
                }
            case CONDITIONS.NPC2_STATE_1:
                {
                    return Inventory.HAS_THE_GUN && !Inventory.HAS_BULLETS;
                }

            case CONDITIONS.NPC2_STATE_2:
                {
                    return Inventory.HAS_BULLETS && Inventory.HAS_THE_GUN;
                }
            case CONDITIONS.NPC3_STATE_0:
                {
                    return !Inventory.TALKED_WITH_X;
                }

            case CONDITIONS.NPC3_STATE_1:
                {
                    return Inventory.TALKED_WITH_X;
                }
            case CONDITIONS.NPC4_STATE_0:
                {
                    return !Inventory.HAS_BULLETS;
                }
            case CONDITIONS.NPC4_STATE_1:
                {
                    return Inventory.HAS_BULLETS && !Inventory.HAS_THE_KEY;
                }
            case CONDITIONS.NPC4_STATE_2:
                {
                    return Inventory.HAS_BULLETS && Inventory.HAS_THE_KEY;
                }
            case CONDITIONS.DOOR:
                {
                    return Inventory.HAS_BULLETS && Inventory.HAS_THE_KEY && Inventory.HAS_THE_GUN && Inventory.TALKED_WITH_X;
                }
            case CONDITIONS.NPC0_STATE_0:
                {
                    return !Inventory.HAS_BULLETS || !Inventory.HAS_THE_KEY || !Inventory.HAS_THE_GUN || !Inventory.TALKED_WITH_X;
                }
            case CONDITIONS.NPC0_STATE_1:
                {
                    return Inventory.HAS_BULLETS || Inventory.HAS_THE_KEY || Inventory.HAS_THE_GUN || Inventory.TALKED_WITH_X;
                }


        }
        return true;
    }

    public bool m_autoLaunch = false;
         public REWARDS reward;
    public CONDITIONS condition = CONDITIONS.NONE;
    public string characterName;
    public string[] dialog;
    public Sprite avatar;

    bool sentEnterEvent;

    void OnTriggerEnter2D(Collider2D other)
    {
        if (DoesMeetCOndition())
        {
            if (m_autoLaunch)
            {
                SNESEvents.SendConversationStartedEvent(this);
            }
            else
            {
                sentEnterEvent = true;
                SNESEvents.SendConversationEnteredEvent(this);
            }
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (sentEnterEvent)
        {
            sentEnterEvent = false;
            SNESEvents.SendConversationExitEvent(this);
        }
        
    }
}
