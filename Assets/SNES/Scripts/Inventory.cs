﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Inventory
{
    public static bool HAS_THE_KEY;
    public static bool HAS_THE_GUN;
    public static bool HAS_BULLETS;
    public static bool TALKED_WITH_X;
}
