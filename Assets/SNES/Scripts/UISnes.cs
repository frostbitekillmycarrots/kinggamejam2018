﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UISnes : MonoBehaviour {

    [SerializeField] GameObject m_pistol;
    [SerializeField] GameObject m_bullets;
    [SerializeField] GameObject m_key;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        m_pistol.gameObject.SetActive(Inventory.HAS_THE_GUN);
        m_bullets.gameObject.SetActive(Inventory.HAS_BULLETS);
        m_key.gameObject.SetActive(Inventory.HAS_THE_KEY);
    }
}
