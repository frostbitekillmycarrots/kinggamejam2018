﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ConversationUI : MonoBehaviour {

    [SerializeField]Image m_avatar;
    [SerializeField] Text m_name;
    [SerializeField] TextWriter m_writer;
    [SerializeField] GameObject m_arrow;
    int m_index = 0;
    Conversation m_conversation;
    bool m_conversationOnGoing;

    void Awake()
    {
        gameObject.SetActive(false);
        SNESEvents.ConversationStarted += SNESEvents_ConversationStarted;
        SNESEvents.ConversationCut += SNESEvents_ConversationCut;
    }


    private void OnDestroy()
    {
        SNESEvents.ConversationStarted -= SNESEvents_ConversationStarted;
        SNESEvents.ConversationCut -= SNESEvents_ConversationCut;
    }
    private void SNESEvents_ConversationStarted(Conversation c)
    {
        gameObject.SetActive(true);
        m_conversation = c;
        m_index = 0;
        m_avatar.sprite = c.avatar;
        m_name.text = c.characterName;

        m_conversationOnGoing = true;
        m_arrow.SetActive(false);
        m_writer.SetText(c.dialog[0], FInishedMessage);
       
    }

    private void SNESEvents_ConversationCut(Conversation c)
    {
       
        if (m_conversationOnGoing)
        {
            m_writer.FinishMessage();
            m_conversationOnGoing = false;
            m_arrow.SetActive(m_index < m_conversation.dialog.Length - 1);
        }
        else
        {
            NextMessage();

        }
    }

    void NextMessage()
    {
        m_index++;

        if (m_index < m_conversation.dialog.Length)
        {
           
            m_conversationOnGoing = true;
            m_arrow.SetActive(false);
            m_writer.SetText(m_conversation.dialog[m_index], FInishedMessage);
        }
        else
        {
            CWSoundManager.Instance.PlayEffect(GameSounds.Instance.UIClick);
            m_conversation.GiveReward();
            gameObject.SetActive(false);
            SNESEvents.SendConversationFinishedEvent(m_conversation);
        }
    }

    void FInishedMessage()
    {
        m_conversationOnGoing = false;
        m_arrow.SetActive(m_index < m_conversation.dialog.Length - 1);
    }
}
