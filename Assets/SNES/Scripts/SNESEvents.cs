﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SNESEvents {

   public delegate void BasicDelegate();
    public delegate void ConversationDelegate(Conversation c);

    public static event ConversationDelegate ConversationEntered;
    public static void SendConversationEnteredEvent(Conversation c)
    {
        if (ConversationEntered != null)
        {
            ConversationEntered(c);
        }
    }
    public static event ConversationDelegate ConversationExit;
    public static void SendConversationExitEvent(Conversation c)
    {
        if (ConversationExit != null)
        {
            ConversationExit(c);
        }
    }
    public static event ConversationDelegate ConversationStarted;
    public static void SendConversationStartedEvent(Conversation c)
    {
        if (ConversationStarted != null)
        {
            ConversationStarted(c);
        }
    }
    public static event ConversationDelegate ConversationFinished;
    public static void SendConversationFinishedEvent(Conversation c)
    {
        if (ConversationFinished != null)
        {
            ConversationFinished(c);
        }
    }
    public static event ConversationDelegate ConversationCut;
    public static void SendConversationCutEvent(Conversation c)
    {
        if (ConversationCut != null)
        {
            ConversationCut(c);
        }
    }

    public static event BasicDelegate FAtherRewarded;
    public static void SendFatherEvent()
    {
        if (FAtherRewarded != null)
        {
            FAtherRewarded();
        }
    }

    public static event BasicDelegate OutOfScreen;
    public static void SendEventOutOfScreen()
    {
        if (OutOfScreen != null)
        {
            OutOfScreen();
        }
    }
}
