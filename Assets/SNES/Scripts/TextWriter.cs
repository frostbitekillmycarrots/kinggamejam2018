﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TextWriter: MonoBehaviour
{
    private float letterPause = 0.025f;
    private string message;
    [SerializeField] Text textComp;
    bool talking;
    bool sound;

    public void FinishMessage()
    {
        StopAllCoroutines();
        textComp.text = message;
    }

    public void SetText(string text, System.Action callback)
    {
        textComp.text = "";
        message = text;
        StartCoroutine(TypeText(callback));
    }

    IEnumerator TypeText(System.Action callback)
    {
        foreach (char letter in message.ToCharArray())
        {
            textComp.text += letter;

            if(sound)
            {
                CWSoundManager.Instance.PlayEffect(GameSounds.Instance.Talk);
            }
            sound = !sound;
            yield return new WaitForSeconds(letterPause);
        }
        callback();
    }
}