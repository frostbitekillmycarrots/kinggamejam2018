﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BattleTransition : MonoBehaviour {

    [SerializeField] GameObject[] m_thingsToDeactivate;
    Image m_image;
    [SerializeField] GameObject m_balttleScene;
    [SerializeField] GameObject m_battleUI;
    [SerializeField] Camera m_camera;
	// Use this for initialization
	void Awake () {
        m_image = GetComponent<Image>();
        SNESEvents.FAtherRewarded += SNESEvents_FAtherRewarded;
	}

    void OnDestroy()
    {
        SNESEvents.FAtherRewarded -= SNESEvents_FAtherRewarded;
    }

    private void SNESEvents_FAtherRewarded()
    {
        CWSoundManager.Instance.PlayMusic(GameSounds.Instance.m_musicBattle);
        gameObject.SetActive(true);
        StartCoroutine(Fill());
    }

    IEnumerator Fill()
    {
        float time = 1f;
        float timer = 0f;

        while (timer < time)
        {
            timer += Time.deltaTime;
            m_image.fillAmount = Mathf.Lerp(0, 1, timer / time);
            yield return null;
        }
        m_image.fillAmount = 1;

        foreach (GameObject go in m_thingsToDeactivate)
        {
            go.SetActive(false);
        }

        m_camera.transform.SetParent(m_balttleScene.transform);
        m_camera.transform.localPosition = new Vector3(0, 0, -10);
        m_camera.orthographicSize = 1.2f;
        m_balttleScene.SetActive(true);
        StartCoroutine(Empty());
    }

    IEnumerator Empty()
    {
        float time = 1f;
        float timer = 0f;
        yield return new WaitForSeconds(0.2f);
        while (timer < time)
        {
            timer += Time.deltaTime;
            m_image.fillAmount = Mathf.Lerp(1, 0, timer / time);
            yield return null;
        }
        m_image.fillAmount = 0;
        m_battleUI.SetActive(true);
    }
    }
