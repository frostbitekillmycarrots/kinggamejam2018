﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ZInput;

public class SNESMainCharacter : MonoBehaviour, ControlListener
{
    Rigidbody2D m_rigidBody;
    const float SPEED = 150f;
    UI.UIAnimator m_uiAnimator;
    SpriteRenderer m_spriteRenderer;

    private const string ANIM_IDLE_DOWN = "Idle_Down";
    private const string ANIM_IDLE_UP = "Idle_Up";
    private const string ANIM_IDLE_SIDE = "Idle_Side";
    private const string ANIM_RUN_DOWN = "Run_Down";
    private const string ANIM_RUN_SIDE = "Run_Side";
    private const string ANIM_RUN_UP = "Run_Up";
    private const string ANIM_SHOOT_SIDE = "Shoot_Side";
    private const string ANIM_ANIM_WIN = "Anim_Win";

    enum LOOK
    {
        Down,
        Up,
        Left,
        Right
    }

    enum STATE
    {
        None,
        Moving,
        Talking,
        Fight
    }
    STATE m_state;
    LOOK m_look;
    Conversation m_currentConversation;

    private void Awake()
    {
        m_rigidBody = GetComponent<Rigidbody2D>();
        m_uiAnimator = GetComponentInChildren<UI.UIAnimator>();
        m_spriteRenderer = GetComponentInChildren<SpriteRenderer>();
        m_uiAnimator.PlayAnimation(ANIM_IDLE_DOWN);
        m_look = LOOK.Down;
        m_state = STATE.None;
        SNESEvents.ConversationEntered += SNESEvents_ConversationEntered;
        SNESEvents.ConversationExit += SNESEvents_ConversationExit;
        SNESEvents.ConversationFinished += SNESEvents_ConversationFinished;
        SNESEvents.FAtherRewarded += SNESEvents_FAtherRewarded;
        SNESEvents.ConversationStarted += SNESEvents_ConversationStarted;
    }

    public void Init()
    {
        m_state = STATE.Moving;
    }

    private void SNESEvents_ConversationStarted(Conversation c)
    {
        if (m_currentConversation == null)
        {
            Move(0, 0);
             m_state = STATE.Talking;
            m_currentConversation = c;
        }
    }

    private void SNESEvents_FAtherRewarded()
    {
        m_state = STATE.Fight;
        m_uiAnimator.PlayAnimation(ANIM_IDLE_DOWN);
    }

    private void OnDestroy()
    {
        SNESEvents.ConversationEntered -= SNESEvents_ConversationEntered;
        SNESEvents.ConversationExit -= SNESEvents_ConversationExit;
        SNESEvents.ConversationFinished -= SNESEvents_ConversationFinished;
        SNESEvents.FAtherRewarded -= SNESEvents_FAtherRewarded;
        SNESEvents.ConversationStarted -= SNESEvents_ConversationStarted;
    }

    private void SNESEvents_ConversationEntered(Conversation c)
    {
        m_currentConversation = c;
    }
    private void SNESEvents_ConversationExit(Conversation c)
    {
        m_currentConversation = null;
    }
    private void SNESEvents_ConversationFinished(Conversation c)
    {
        StartCoroutine(OneFrameDElayedMoving());
    }

    IEnumerator OneFrameDElayedMoving()
    {
        
        yield return null;
        if (m_state != STATE.Fight)
        {
            m_state = STATE.Moving;
        }
        
    }

    // Use this for initialization
    void Start () {
        InputBinder.BindControlToListener(CONTROLLER.KEYBOARD, this);

    }
	
	// Update is called once per frame
	void Update () {
     
    }

    void MapMoveControl(float x, float y)
    {
        if (m_state != STATE.Moving)
        {
            return;
        }
        Vector2 speed = new Vector2(x, y);
        speed.Normalize();
        speed = speed * SPEED * Time.deltaTime;
        m_rigidBody.velocity = speed;
        m_spriteRenderer.flipX = false;

        if (x == 0 && y == 0)
        {
            if (m_look == LOOK.Down)
            {
                m_uiAnimator.PlayAnimation(ANIM_IDLE_DOWN);
            }
            else if (m_look == LOOK.Up)
            {
                m_uiAnimator.PlayAnimation(ANIM_IDLE_UP);
            }
            else if (m_look == LOOK.Left)
            {
                m_spriteRenderer.flipX = true;
                m_uiAnimator.PlayAnimation(ANIM_IDLE_SIDE);
            }
            else if (m_look == LOOK.Right)
            {
                m_uiAnimator.PlayAnimation(ANIM_IDLE_SIDE);
            }
        }
        else
        {
            Vector3 position = transform.position;
            position.z = position.y;
            transform.position = position;

            if (Mathf.Abs(x) > Mathf.Abs(y)) 
            {
                if (x > 0)
                {
                    m_look = LOOK.Right;
                    m_uiAnimator.PlayAnimation(ANIM_RUN_SIDE);
                }
                else
                {
                    m_spriteRenderer.flipX = true;
                    m_look = LOOK.Left;
                    m_uiAnimator.PlayAnimation(ANIM_RUN_SIDE);
                }
            }
            else
            {
                if (y > 0)
                {
                    m_look = LOOK.Up;
                    m_uiAnimator.PlayAnimation(ANIM_RUN_UP);
                }
                else
                {
                    m_look = LOOK.Down;
                    m_uiAnimator.PlayAnimation(ANIM_RUN_DOWN);
                }

            }
        }
    }

    private void Move(float x, float y)
    {
        MapMoveControl(x,y);
    }

    void ActionMoveControl()
    {
        if (m_state != STATE.Moving)
        {
            return;
        }

        if (m_currentConversation != null)
        {
            Move(0, 0);
            m_state = STATE.Talking;
            SNESEvents.SendConversationStartedEvent(m_currentConversation);
        }
    }

    void ActionTalkControl()
    {
        if (m_state != STATE.Talking)
        {
            return;
        }

        if (m_currentConversation != null)
        {
            SNESEvents.SendConversationCutEvent(m_currentConversation);
        }
    }

    private void PressedAction()
    {
        ActionTalkControl();
        ActionMoveControl();
       
    }








    //////////
    //INPUT
    //////////
    public void AnyButtonPressed(CONTROLLER controller)
    {

    }

    public void AnyButtonReleased(CONTROLLER controller)
    {

    }

    public void Axis(float x, float y)
    {
        Move(x, y);
    }

    public void ButtonAPressed()
    {
        PressedAction();
    }

    public void ButtonAReleased()
    {

    }

    public void ButtonBPressed()
    {

    }

    public void ButtonBReleased()
    {

    }

    public void ButtonLPressed()
    {

    }

    public void ButtonLReleased()
    {

    }

    public void ButtonRPressed()
    {

    }

    public void ButtonRReleased()
    {

    }

    public void ButtonXPressed()
    {

    }

    public void ButtonXReleased()
    {

    }

    public void ButtonYPressed()
    {

    }

    public void ButtonYReleased()
    {

    }
}

