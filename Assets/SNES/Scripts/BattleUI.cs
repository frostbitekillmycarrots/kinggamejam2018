﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ZInput;

public class BattleUI : MonoBehaviour{

    private const string ANIM_IDLEBATTLE = "IdleBattle";
    private const string ANIM_BATTLE = "battle";


    public UI.UIAnimator battle;
    public GameObject nope;
    public GameObject enemy;
    public GameObject barrel;
    public GameObject background;
    bool attackEnemy = true;
    bool disabled = false;

    public GameObject[] thingsThatDissapearAfterAnimation;

    // Use this for initialization
    void Awake()
    {
        gameObject.SetActive(false);
    }
        void Start () {
		
	}
	
//    public void ClickedBarrel()
//    {
//        attackEnemy = false;
//        background.transform.SetParent(barrel.transform, false);
//        background.SetActive(true);
//        Barrel();
//    }
//
//    public void ClickedEnemy()
//    {
//        attackEnemy = true;
//        background.transform.SetParent(enemy.transform,false);
//        background.SetActive(true);
//        nope.SetActive(true);
//    }

    void Barrel()
    {
        disabled = true;
        CWSoundManager.Instance.PlayEffect(GameSounds.Instance.Shoot);
        battle.PlayAnimation(ANIM_BATTLE, () =>
        {
            foreach (GameObject go in thingsThatDissapearAfterAnimation)
            {
                go.SetActive(false);
                SNESEvents.SendEventOutOfScreen();
                CWSoundManager.Instance.StopMusic();
            }
        });
        nope.SetActive(false);
    }

    IEnumerator BoomSound()
    {
        yield return new WaitForSeconds(0.2f);
        CWSoundManager.Instance.PlayEffect(GameSounds.Instance.Explosion);
    }

    // Update is called once per frame
    void Update () {
        if (disabled)
            return;

		if (Input.GetKeyDown(KeyCode.DownArrow) || Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.W))
        {
            CWSoundManager.Instance.PlayEffect(GameSounds.Instance.UIClick);
            if (attackEnemy)
            {

                background.transform.SetParent(barrel.transform, false);
            }
            else
            {
                background.transform.SetParent(enemy.transform, false);
            }
            attackEnemy = !attackEnemy;
        }

		if (Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(0))
        {
            CWSoundManager.Instance.PlayEffect(GameSounds.Instance.UIClick);
            if (!attackEnemy)
            {
                Barrel();
            }
            else
            {
                nope.SetActive(true);
            }
        }
	}
}
