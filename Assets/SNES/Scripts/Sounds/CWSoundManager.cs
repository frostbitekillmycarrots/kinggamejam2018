﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CWSoundManager : MonoBehaviour {

	[SerializeField] AudioSource m_musicSource;

	AudioSource[] m_sources;
	int m_currentSource =0 ;

	float m_initialPitch = 1.0f;
	float m_finalPitch = 1.5f;
	float m_finalLevel = 10;

	private const string MUTE_EFFECTS_PREF = "MuteEffects";
	private const string MUTE_MUSIC_PREF = "MuteMusic";

	private static CWSoundManager s_instance;
	private bool m_muteMusic;
	private bool m_muteEffects;

	public static CWSoundManager Instance { get { return s_instance; } }

	void Awake()
	{
		if (s_instance == null)
		{
			s_instance = this;

			m_sources = GetComponents<AudioSource>();

			MuteEffects(PlayerPrefs.GetInt(MUTE_EFFECTS_PREF) > 0);
			MuteMusic(PlayerPrefs.GetInt(MUTE_MUSIC_PREF) > 0);
			m_musicSource.pitch = m_initialPitch;

			DontDestroyOnLoad(gameObject);
		}
		else
		{
			Destroy(gameObject);
		}
	}

	public void PlayEffect (AudioClip clip)
	{
		if (!m_muteEffects && clip != null)
		{
			AudioSource source = m_sources[m_currentSource];

			m_currentSource++;
			if (m_currentSource >= m_sources.Length)
			{
				m_currentSource = 0;
			}
			
			source.clip = clip;
			source.Play();
		}
	}

	public void PlayMusic(AudioClip clip, bool loop = true)
	{
		Debug.Log("kaka");
		if (clip != null)
		{
			m_musicSource.clip = clip;
			m_musicSource.loop = loop;
			if (!m_muteMusic)
			{
				m_musicSource.Play();
			}
		}
	}

	public void StopMusic()
	{
		m_musicSource.Stop();
	}

	public void MuteMusic(bool mute)
	{
		m_muteMusic = mute;
		PlayerPrefs.SetInt(MUTE_MUSIC_PREF, m_muteMusic ? 1 : 0);

		if (mute)
		{
			m_musicSource.Stop();
		}
		else
		{
			m_musicSource.Play();
		}
	}

	public void MuteEffects(bool mute)
	{
		m_muteEffects = mute;
		PlayerPrefs.SetInt(MUTE_EFFECTS_PREF, m_muteEffects ? 1 : 0);
	
	}

	public void MuteAllToggle()
	{
		MuteEffects(PlayerPrefs.GetInt(MUTE_EFFECTS_PREF) > 0 ? false : true);
		MuteMusic(PlayerPrefs.GetInt(MUTE_MUSIC_PREF) > 0 ? false : true);
	}

	public bool IsEffectMuted()
	{
		return m_muteEffects;
	}

	public bool IsMusicMuted()
	{
		return m_muteMusic;
	}


	public bool IsMusicPlaying()
	{
		return m_musicSource.isPlaying;
	}
}
