﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSounds : MonoBehaviour
{
	//Effects
	public AudioClip Explosion;
	public  AudioClip Progress;
    public AudioClip Shoot;
    public AudioClip Talk;
    public AudioClip UIClick;
    public AudioClip IntoCOnsole;
    public AudioClip Jump;

    public AudioClip m_musicIntro;
    public AudioClip m_musicMain;
    public AudioClip m_musicBattle;
    public AudioClip m_musicCar;

    private static GameSounds s_instance;
	public static GameSounds Instance { get { return s_instance; } }

	void Awake()
	{
		if (s_instance == null)
		{
			s_instance = this;
			DontDestroyOnLoad(gameObject);
		}
		else
		{
			Destroy(gameObject);
		}
	}

	public static void  StopMusic()
	{
		s_instance.StopAllCoroutines();
		CWSoundManager.Instance.StopMusic();
	}

	public static void PlayMusic()
	{
		CWSoundManager.Instance.PlayMusic(s_instance.m_musicIntro, false);
		s_instance.StartCoroutine(s_instance.PlayMusicAtEndIntro());
	}

	IEnumerator PlayMusicAtEndIntro()
	{
		while (CWSoundManager.Instance.IsMusicPlaying())
		{
			yield return new WaitForEndOfFrame ();
		}
		CWSoundManager.Instance.PlayMusic(s_instance.m_musicMain);
	}
}
