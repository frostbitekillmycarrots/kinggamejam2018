﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Core.Utils;


namespace Utils
{
	public class RenderCameraWorld : MonoSingleton<RenderCameraWorld>
	{
		[SerializeField] private Camera m_CameraRenderer;
	
		private RenderTexture m_renderTexture;
		private int m_outputWidth;
		private int m_outputHeight;

		protected override void Init()
		{
			m_CameraRenderer.enabled = false;
		}

		public IEnumerator SetupCapture(int width, int height)
		{
			m_renderTexture = new RenderTexture (width, height, 24);

			float orthFactor = (float)m_CameraRenderer.pixelHeight / (float)height;

			m_outputWidth = width;
			m_outputHeight = height;
			m_CameraRenderer.targetTexture = m_renderTexture;

			m_CameraRenderer.orthographicSize = (float)(m_CameraRenderer.orthographicSize / orthFactor);

			yield return new WaitForEndOfFrame ();
		}

		public Texture2D RetrieveTexture()
		{
			m_CameraRenderer.Render();
			RenderTexture.active = m_renderTexture;

			Texture2D outputTexture = new Texture2D (m_outputWidth, m_outputHeight, TextureFormat.ARGB32, false);
			outputTexture.ReadPixels(new Rect (0, 0, m_outputWidth, m_outputHeight), 0, 0); // you get the center section

			outputTexture.Apply();

			// Cleanup
			RenderTexture.active = null;
			RenderTexture.Destroy(m_renderTexture);
			m_renderTexture = null;

			m_CameraRenderer.targetTexture = null;
			m_CameraRenderer.enabled = false;

			m_CameraRenderer.orthographicSize = Camera.main.orthographicSize;

			return outputTexture;
		}
	}
}
