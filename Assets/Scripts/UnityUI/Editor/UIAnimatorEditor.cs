﻿using UnityEditor;
using UnityEngine;
using Core;
using System.Collections.Generic;
using UI;
using UnityEditor.Animations;

[CustomEditor(typeof(UI.UIAnimator), true)]
public class UIAnimatorEditor : Editor
{
	string animations = "Click button";

	public override void OnInspectorGUI()
	{
		UI.UIAnimator element = (UI.UIAnimator)target;

		DrawDefaultInspector();

		if (GUILayout.Button("Auto read animations"))
		{
			GetAnimations();
		}
		if (GUILayout.Button("Generate code"))
		{
			List<UIAnimator.UIAnimState> animationsList = element.GetAnimationList();
			animations = "";
			for (int i = 0; i < animationsList.Count; ++i)
			{
				string animation = animationsList[i].animationName;
				animations += "private const string ANIM_" + animation.ToUpper().Replace('-','_') + " = \"" + animation + "\"; \n";

			}
		}

		if (GUILayout.Button("Hide code"))
		{
			animations = "Hello!";
		}

		EditorGUILayout.TextArea(animations);
	}

	void GetAnimations()
	{
		UI.UIAnimator element = (UI.UIAnimator)target;
		Animator animator = element.GetComponent<Animator>();
		AnimatorController ac = animator.runtimeAnimatorController as AnimatorController;
		if (ac == null)
		{
			AnimatorOverrideController animatorOverrideController;
			animatorOverrideController = new AnimatorOverrideController (animator.runtimeAnimatorController);
			ac = animatorOverrideController.runtimeAnimatorController as AnimatorController;
		}			

		if (ac != null)
		{
			UnityEditor.Animations.AnimatorControllerLayer[] layers = ac.layers;
			AnimatorStateMachine sm = layers[0].stateMachine;
			ChildAnimatorState[] states = sm.states;
			for (int i = 0; i < states.Length; ++i)
			{
				element.AddState(states[i].state.name);
			}

			ReloadPreviewInstances();
		}
	}
}