﻿using System;
using UnityEngine;

namespace Core
{
	namespace Utils
	{
		/// <summary>
		/// Base behaviour.
		/// All classes which inherit from MonoBehaviour should inherit from this class instead
		/// </summary>

		public class BaseBehaviour : MonoBehaviour
		{
			protected bool m_hasBeenDisabled = false;

			protected virtual void OnDestroy()
			{
			}

			protected virtual void Awake()
			{
			}

			protected virtual void Start()
			{
			}

			protected virtual void OnEnable()
			{
				if (m_hasBeenDisabled)
				{
					OnReEnable();
				}
			}

			protected virtual void OnReEnable()
			{
				// THIS CAN BE OVERRIDDEN TO DO WORK ONLY FOR NON FIRST TIME ENABLE
			}

			protected virtual void OnDisable()
			{
				m_hasBeenDisabled = true;
			}

			protected virtual void Update()
			{
			}

			protected virtual void Invoke(Action action, float delay)
			{
				Invoke(action.Method.Name, delay);
			}

			protected virtual void InvokeRepeating(Action action, float delay, float repeat)
			{
				InvokeRepeating(action.Method.Name, delay, repeat);
			}

			protected virtual void CancelInvoke(Action action)
			{
				CancelInvoke(action.Method.Name);
			}

			protected T GetComponentSafe<T>() where T : Component
			{
				T t = GetComponent<T>();

				if (t == null)
				{
					Debug.LogError("Could not find component of type: " + typeof(T));
				}
				return t;
			}

			#if UNITY_EDITOR
			protected T AddOrReplaceImmediate<T>() where T : Component
			{
				T t = GetComponent<T>();

				if (t != null)
				{
					DestroyImmediate(t);
				}

				return gameObject.AddComponent<T>();
			}
			#endif
		}
	}
}