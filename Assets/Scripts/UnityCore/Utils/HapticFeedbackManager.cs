﻿using UnityEngine;
using System.Collections;
using Core.Utils;
using System;
using Core.Debugging;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.IO;

#if UNITY_IOS
using System.Runtime.InteropServices;
using UnityEngine.iOS;
#endif
namespace Core
{
	namespace Utils
	{
		public class HapticFeedbackManager : MonoSingleton<HapticFeedbackManager>
		{
			public static HapticFeedback SELECTION_HAPTIC = new HapticFeedback (0);
			public static HapticFeedback LIGHT_IMPACT_HAPTIC = new HapticFeedback (1);
			public static HapticFeedback MEDIUM_IMPACT_HAPTIC = new HapticFeedback (2);
			public static HapticFeedback HEAVY_IMPACT_HAPTIC = new HapticFeedback (3);
			public static HapticNotifier NOTIFICATION_HAPTIC = new HapticNotifier (4);

			private static string DONT_TRIGGER_HAPTIC_TAG = "DontTriggerHaptic";

			public enum Type
			{
				SELECTION,
				LIGHT_IMPACT,
				MEDIUM_IMPACT,
				HEAVY_IMPACT
			}

			#if (UNITY_IPHONE || UNITY_IOS)
			DeviceGeneration[] m_tapticPhones = { 
				DeviceGeneration.iPhone7, 
				DeviceGeneration.iPhone7Plus,
				DeviceGeneration.iPhone8, 
				DeviceGeneration.iPhone8Plus,
				DeviceGeneration.iPhoneX
			};
			#endif

			private bool m_hapticEnabled = true;

			protected override void Start()
			{
				base.Start();
			}

			protected override void Init()
			{
				ReadHapticSettingsFromBucket();
			}

			public bool HapticEnabled
			{
				get { return m_hapticEnabled; }
			}

			public void SetHapticEnable(bool enable, bool save = true)
			{
				m_hapticEnabled = enable;
				if (save)
				{
					WriteHapticSettingsToBucket();
				}
			}

			public bool DeviceSupportsHaptic()
			{
				#if (UNITY_IPHONE || UNITY_IOS)
				DeviceGeneration generation = UnityEngine.iOS.Device.generation;
				foreach(DeviceGeneration gen in m_tapticPhones)
				{
					if(gen == generation)
					{
						return true;
					}
				}
				return false;
				#else
				return false;
				#endif
			}

			private void ReadHapticSettingsFromBucket()
			{
			}

			private void WriteHapticSettingsToBucket()
			{
			}








			public class HapticFeedback
			{
				protected int m_id;
				protected bool m_isReleased;
				protected bool m_isPrepared;

				public HapticFeedback(int id)
				{
					m_id = id;
					m_isReleased = false;
					m_isPrepared = false;
					HapticFeedbackManager.InstantiateFeedbackGenerator(m_id);
				}

				public void Trigger(bool stayPrepared = false)
				{
					if (HapticFeedbackManager.Instance.HapticEnabled)
					{
						if (m_isReleased)
						{
							HapticFeedbackManager.InstantiateFeedbackGenerator (m_id);
							Debugger.Log ("Attempted to trigger Feedback Generator " + m_id + "which has been released, creating new generator");
						}

						HapticFeedbackManager.TriggerFeedbackGenerator (m_id);
						m_isPrepared = false;

						if (stayPrepared)
						{
							Prepare ();	
						}
					}
				}

				public void Release()
				{
					if (HapticFeedbackManager.Instance.HapticEnabled)
					{
						HapticFeedbackManager.ReleaseFeedbackGenerator (m_id);
						m_isReleased = true;
					}
				}

				public void Prepare()
				{
					if (HapticFeedbackManager.Instance.HapticEnabled)
					{
						if (m_isReleased)
						{
							HapticFeedbackManager.InstantiateFeedbackGenerator (m_id);
						}

						HapticFeedbackManager.PrepareFeedbackGenerator (m_id);
						m_isPrepared = true;
					}
				}
			}







			public class HapticNotifier : HapticFeedback
			{
				public enum Type
				{
					SUCCESS,
					WARNING,
					FAIL
				}

				public HapticNotifier(int id) : base(id)
				{

				}

				public void Trigger(Type type, bool stayPrepared = false)
				{
					m_id += (int)type;
					base.Trigger(stayPrepared);
					m_id -= (int)type;
				}
			}





			#if (UNITY_IOS && !UNITY_EDITOR)
			[DllImport ("__Internal")] private static extern void _instantiateFeedbackGenerator(int id);
			[DllImport ("__Internal")] private static extern void _prepareFeedbackGenerator(int id);
			[DllImport ("__Internal")] private static extern void _triggerFeedbackGenerator(int id);
			[DllImport ("__Internal")] private static extern void _releaseFeedbackGenerator(int id);
			#else
			private static void _instantiateFeedbackGenerator(int id)
			{
			}
			private static void _prepareFeedbackGenerator(int id)
			{
			}
			private static void _triggerFeedbackGenerator(int id)
			{
			}
			private static void _releaseFeedbackGenerator(int id)
			{
			}
			#endif

			private static void InstantiateFeedbackGenerator(int id)
			{
				_instantiateFeedbackGenerator(id);
			}
			private static void PrepareFeedbackGenerator(int id)
			{
				_prepareFeedbackGenerator(id);
			}
			private static void TriggerFeedbackGenerator(int id)
			{
				_triggerFeedbackGenerator(id);
			}
			private static void ReleaseFeedbackGenerator(int id)
			{
				_releaseFeedbackGenerator(id);
			}

			protected override void Update()
			{
				if (Input.GetMouseButtonDown(0))
				{
					GameObject selected = EventSystem.current.currentSelectedGameObject;
					if (selected != null)
					{
						if (selected.GetComponent(typeof(Selectable)) != null)
						{
							if ((selected.tag != DONT_TRIGGER_HAPTIC_TAG) && HapticEnabled)
							{
								HapticFeedbackManager.SELECTION_HAPTIC.Trigger();
							}
						}
					}
				}
			}
		}
	}
}
