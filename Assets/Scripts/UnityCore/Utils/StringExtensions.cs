﻿using UnityEngine;
using System.Security.Cryptography;
using System.Text;

namespace Core
{
	namespace Utils
	{

		static class StringExtensions
		{
			public static string RemoveSpecialCharacters(this string text)
			{
				string toText = System.Text.RegularExpressions.Regex.Replace(text, "[^\\w\\s]", "");
				toText = System.Text.RegularExpressions.Regex.Replace(toText, "_", "");
				return toText;
			}

			public static string ReplaceSpecialCharacters(this string text, string replacement = "_")
			{
				string toText = System.Text.RegularExpressions.Regex.Replace(text, "[^\\w\\s]", replacement);
				return toText;
			}

			public static bool ContainsAToZCharacter(this string text)
			{
				int letterCount = System.Text.RegularExpressions.Regex.Matches(text, @"[a-zA-Z]").Count;
				return letterCount > 0;
			}

			public static string RemoveNonAToZ(this string text)
			{
				string toText = System.Text.RegularExpressions.Regex.Replace(text, "[^a-zA-Z\\s]", "");
				return toText;
			}

			public static string CalculateMD5Hash(this string input)
			{
				// step 1, calculate MD5 hash from input
				MD5 md5 = System.Security.Cryptography.MD5.Create();
				byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
				byte[] hash = md5.ComputeHash(inputBytes);

				// step 2, convert byte array to hex string
				StringBuilder sb = new StringBuilder ();
				for (int i = 0; i < hash.Length; i++)
				{
					sb.Append(hash[i].ToString("X2"));
				}
				return sb.ToString();
			}

			public static bool ContainsSurrogatePair(this string obj)
			{
				bool ret = false;

				foreach (char element in obj)
				{
					if (char.IsHighSurrogate(element))
					{
						ret = true;
						break;
					}
				}

				return ret;
			}

			public static string AddCommas(this string obj)
			{
				string result = "";
				int numsAdded = 0;
				char[] chars = obj.ToCharArray();
				for (int i = chars.Length - 1; i >= 0; i--)
				{
					result = chars[i].ToString() + result;
					numsAdded++;

					if (numsAdded == 3)
					{
						if (i > 0)
						{
							result = "," + result;
						}

						numsAdded = 0;
					}
				}

				return result;
			}
		}
	}
}