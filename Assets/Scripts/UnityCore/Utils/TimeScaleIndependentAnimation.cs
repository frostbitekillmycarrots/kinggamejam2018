﻿/*
 * Original code got from Asteroid Base (Lovers in a dangerous space time)
 * and readapted by Manuel for Skidz
 */

using UnityEngine;
using System.Collections;
using UI;

public class TimeScaleIndependentAnimation : TimeScaleIndependentUpdate
{
	//inspector fields
	public bool m_playOnStart;

	//private fields
	System.Action m_currentCompletionHandler;
	float m_elapsedTime;
	bool m_playing;
	Animator m_animation;

	float m_animationLength;
	string m_animationName;
	bool m_loop;

	Coroutine m_coroutine = null;

	public bool IsPlaying{ get { return m_playing; } }

	// note - you may have problems with this where there are animations that have 0 length.
	// try to make sure that all animations (even ones that are just a static frame), have a non-zero length (e.g. minimum 0.1s)

	public void Play(string animationName, System.Action completionHandler = null)
	{
		m_currentCompletionHandler = completionHandler;
		m_elapsedTime = 0;

		m_animationName = animationName;

		if (m_animation == null)
		{
			m_animation = GetComponent<Animator>();
		}
		m_animation.Play(m_animationName, 0, 0);

		if (m_coroutine != null)
		{
			StopCoroutine (m_coroutine);
		}
		m_coroutine = StartCoroutine(OneFrameDelayedAction());
	}
		
	IEnumerator OneFrameDelayedAction()
	{
		yield return new WaitForEndOfFrame();
		AnimatorStateInfo currentState = m_animation.GetCurrentAnimatorStateInfo(0);
		m_animationLength = currentState.length;
		m_loop = currentState.loop;
		m_playing = true;
		m_coroutine = null;
	}

	protected override void Update()
	{
		base.Update();

		if (m_playing)
		{
			m_elapsedTime += deltaTime;
			float normalizedTime = m_elapsedTime / m_animationLength;

			if (m_elapsedTime >= m_animationLength)
			{
				m_playing = false;
				m_elapsedTime = 0;

				if (m_currentCompletionHandler != null)
				{
					m_currentCompletionHandler();
				}

				if (m_loop)
				{
					Play(m_animationName, m_currentCompletionHandler);
				}
			}
			else
			{
				m_animation.Play(m_animationName, -1, normalizedTime);
			}
		}
	}

}