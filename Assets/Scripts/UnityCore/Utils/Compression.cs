﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.IO.Compression;
using System.Text;
using System;

public static class Compression
{
	public static string DecompressStringGZip(string compressedString)
	{
		byte[] inputBytes = Convert.FromBase64String(compressedString);
		MemoryStream inputStream = new MemoryStream (inputBytes);

		GZipStream gZipStream = new GZipStream (inputStream, CompressionMode.Decompress);
		StreamReader reader = new StreamReader (gZipStream);

		string decompressedString = reader.ReadToEnd();

		return decompressedString;
	}

	public static string CompressStringGZip(string inputString)
	{
		byte[] inputBytes = Encoding.UTF8.GetBytes(inputString);

		MemoryStream outputStream = new MemoryStream();

		GZipStream gZipStreamStream = new GZipStream (outputStream, CompressionMode.Compress, true);
		gZipStreamStream.Write(inputBytes, 0, inputBytes.Length);
		gZipStreamStream.Close();

		string compressedString = Convert.ToBase64String(outputStream.ToArray());

		return compressedString;
	}
}
