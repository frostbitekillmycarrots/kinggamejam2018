using System;

public class SharedSystems
{
	public enum Systems
	{
		KANGA,
		OS_MESSAGES,
		HTTP_REQUEST,
		HTTP_RESPONSE,
		HTTP_PRETTY_RESPONSE,
		SERVER_OBJECT,
		SERVER_CACHE,
		ANALYTICS,
		UA_ANALYTICS,
		AUDIO,
		SCREEN_MANAGER,
		POPUP_MANAGER,
		DRAG_AND_DROP,
		QUEST,
		UIIMAGE_PICKER,
		CAMERA_FEED,
		CORE,
		WEB_IMAGE,
		FACEBOOK_HELPER,
		MARKET,
		ADS,
		TEXT_MANAGER,
		APPS_FLYER,
		DEBUG_UTILS,
		MENU_MEDIATOR,
		NOTIFICATIONS,
		SAVE_DATA,
		EVERYPLAY,
		CROSS_PROMOTION,
		GAME_SETTINGS,
		MUTATION_HISTORY,
		SPECIAL_OFFER,
		MAX_SYSTEMS
	}
}