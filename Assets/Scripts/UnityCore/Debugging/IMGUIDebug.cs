﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Core.Utils;
using Core.Debugging;
using UnityEngine.EventSystems;

public class IMGUIDebug : BaseBehaviour
{
	private const int TOGGLE_WIDTH = 25;

	private List<string> m_logNames = new List<string>(){"UNITY"};
	private List<string> m_logContents = new List<string>(){""};

	private bool m_display = false;
	private bool m_started = false;
	private int m_indexToShow = -1;

	[SerializeField] private int m_max = 16382;
	[SerializeField] private Vector2 m_scrollPosition;
	[SerializeField] private float m_timeScale;

	protected override void Start()
	{
		m_started=true;
		UnityEngine.Application.logMessageReceived += HandleLog;
		DontDestroyOnLoad(gameObject);
	}

	protected override void OnDestroy()
	{
		UnityEngine.Application.logMessageReceived += HandleLog;
	}

	private void HandleLog(string logString, string stackTrace, LogType type) 
	{
		m_logContents[0] += type.ToString() + " : " + logString + "\n" + "Stack Trace : " + stackTrace + "\n\n";
	}

	public void Log(string name, string log)
	{
		if (!m_logNames.Contains(name))
		{
			m_logNames.Add(name);
			m_logContents.Add(log);
		}
		else
		{
			int i=m_logNames.IndexOf(name);
			string s = m_logContents[i] + "\n" + log;
			m_logContents[i]=s;
		}
	}

	private void OnGUI() 
	{
		if (!m_started) return;
		
		ShowToggle();

		if (m_display)
		{
			ShowBody();
		}
    }

	private void ShowToggle()
	{
		GUILayout.BeginHorizontal();
		GUILayout.FlexibleSpace();
		GUILayout.Space(Screen.width-TOGGLE_WIDTH);
		if (GUILayout.Button("", GUILayout.Width(TOGGLE_WIDTH)))
		{
			m_display=!m_display;
			if (Time.timeScale!=0f)
			{
				m_timeScale=Time.timeScale;
			}
			Time.timeScale=m_display?0f:m_timeScale;
		}
		GUILayout.EndHorizontal();
	}

	private void ShowBody()
	{
		GUIStyle style = GUI.skin.button;
		style.alignment = TextAnchor.MiddleLeft;
		m_indexToShow = GUILayout.SelectionGrid(m_indexToShow, m_logNames.ToArray(), 3, style);

		if (m_indexToShow >= 0)
		{
			m_scrollPosition = GUILayout.BeginScrollView(m_scrollPosition);
			string s = m_logContents[m_indexToShow];
			
			for (int i = 0; i < s.Length ; i += m_max)
			{
				int size = m_max;
				if (i + size > s.Length) size = s.Length  - i;
				#if !UNITY_EDITOR
				GUILayout.TextArea(s.Substring(i, size));
				#else
				if (GUILayout.Button(s.Substring(i, size)))
				{
					GUIUtility.systemCopyBuffer=m_logContents[m_indexToShow];
				}
				#endif
			}
			GUILayout.EndScrollView();
		}
	}	
}
