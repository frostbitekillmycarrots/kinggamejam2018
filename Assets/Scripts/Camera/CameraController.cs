﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Core.Utils;
using UI;

public class CameraController : BaseBehaviour
{

	private const string ANIM_CAMINTRO = "CamIntro"; 
	private const string ANIM_NONE = "None"; 

	[SerializeField] private UIAnimator m_animator;
	[SerializeField] private GameObject m_controls;

	public enum RotationAxes { MouseXAndY = 0, MouseX = 1, MouseY = 2 }
	public RotationAxes axes = RotationAxes.MouseXAndY;
	public float sensitivityX = 15f;
	public float sensitivityY = 15f;
	public float minimumX = -360f;
	public float maximumX = 360f;
	public float minimumY = -60f;
	public float maximumY = 60f;
	float rotationX = 0f;
	float rotationY = 0f;

	GameBoyPlayer3DWorldC m_player;


	// Amplitude of the shake. A larger value shakes the camera harder.
	public float shakeAmount = 0.7f;

	private bool m_controlEnabled = false;

	Quaternion originalRotation = Quaternion.identity;

	protected override void Awake ()
	{
		base.Awake ();
        Application.targetFrameRate = 60;
	}

	public void OnControlsTapped()
	{
		m_controls.SetActive (false);
		m_animator.PlayAnimation(ANIM_CAMINTRO, OnIntroFinished);
	}

	void OnIntroFinished()
	{
		originalRotation = transform.rotation;

		m_animator.GetAnimator ().enabled = false;
		m_controlEnabled = true;
	}

	public void LerpToIdentity()
	{
		m_controlEnabled = false;
		rotationX = 0f;
		rotationY = 0f;
		StartCoroutine (LerpToIdentityOverTime (2f));


	}

	public void SetFollowPlayer(GameBoyPlayer3DWorldC player)
	{
		rotationX = 0f;
		rotationY = 0f;
		originalRotation = transform.rotation;
		m_player = player;
	}

	IEnumerator LerpToIdentityOverTime(float time)
	{
		float timer = 0;
		Quaternion rot = transform.rotation;

		while (timer < time) 
		{
			timer += Time.deltaTime;

			transform.rotation = Quaternion.Lerp (rot, Quaternion.identity, timer / time);

			yield return null;
		}
		m_controlEnabled = true;
		originalRotation = transform.rotation;
	}

	public void SetMegaDrive()
	{
		rotationX = 0f;
		rotationY = 0f;
		originalRotation = transform.rotation;
		m_player = null;
	}

	public void ShakeCamera()
	{
		StartCoroutine (ShakeCameraCoroutine (0.5f));
	}

	IEnumerator ShakeCameraCoroutine(float duration)
	{
		float timer = 0f;

		Vector3 originalPos = transform.position;

		while (timer < duration) 
		{
			timer += Time.deltaTime;

			transform.localPosition = originalPos + Random.insideUnitSphere * shakeAmount;

			yield return null;
		}

		transform.localPosition = originalPos;
	}

	protected override void Update ()
	{
		if (!m_controlEnabled)
		{
			return;
		}

		if (m_player != null) {
			originalRotation = Quaternion.Lerp(originalRotation, Quaternion.LookRotation (m_player.transform.position - transform.position), 0.1f);		
		}

		if (axes == RotationAxes.MouseXAndY)
		{
			// Read the mouse input axis
			rotationX += Input.GetAxis("Mouse X") * sensitivityX;
			rotationY += Input.GetAxis("Mouse Y") * sensitivityY;

			rotationX = ClampAngle (rotationX, minimumX, maximumX);
			rotationY = ClampAngle (rotationY, minimumY, maximumY);

			Quaternion xQuaternion = Quaternion.AngleAxis (rotationX, Vector3.up);
			Quaternion yQuaternion = Quaternion.AngleAxis (rotationY, -Vector3.right);

			transform.localRotation = originalRotation * xQuaternion * yQuaternion;
		}
		else if (axes == RotationAxes.MouseX)
		{
			rotationX += Input.GetAxis("Mouse X") * sensitivityX;
			rotationX = ClampAngle (rotationX, minimumX, maximumX);

			Quaternion xQuaternion = Quaternion.AngleAxis (rotationX, Vector3.up);

			transform.localRotation = originalRotation * xQuaternion;
		}
		else
		{
			rotationY += Input.GetAxis("Mouse Y") * sensitivityY;
			rotationY = ClampAngle (rotationY, minimumY, maximumY);

			Quaternion yQuaternion = Quaternion.AngleAxis (-rotationY, Vector3.right);

			transform.localRotation = originalRotation * yQuaternion;
		}
	}

	public static float ClampAngle (float angle, float min, float max)
	{
		if (angle < -360f) 
		{
			angle += 360f;
		}

		if (angle > 360f) 
		{
			angle -= 360f;
		}

		return Mathf.Clamp (angle, min, max);
	}
}