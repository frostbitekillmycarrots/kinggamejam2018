﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FZeroCam : MonoBehaviour
{
	[SerializeField] private Ship m_ship;
	[SerializeField] private float m_smoothTime;
	[SerializeField] private Text m_countdown;
	[SerializeField] private GameObject m_youDidIt;
	[SerializeField] private GameObject m_outro;

	private Vector3 m_velocity = Vector3.zero;
	bool m_started = false;
	float m_timer = 0f;

	void Start()
	{
        
    }

	public void StartIntro()
	{
		StartCoroutine (Intro ());
	}

	public void StopTimer()
	{
		m_started = false;
		m_youDidIt.SetActive (true);
		m_outro.SetActive (true);
	}

	IEnumerator Intro()
	{
		float timer = 0;

		float length = 4f;

		while (timer < length) 
		{
			timer += Time.deltaTime;

			m_countdown.text = ((int)4f - timer).ToString ("#");

			if ((int)timer == 4) {
				m_countdown.text = "GO!";
				m_started = true;
				m_ship.SetGo ();
			}

			Vector3 offset = (m_ship.transform.forward * (-6f - (length -timer))) + (Vector3.up * (2f + (length -timer)));

			Quaternion rot = Quaternion.AngleAxis (timer * 90f, m_ship.transform.up);

			offset = rot * offset;

			transform.position = m_ship.transform.position + offset;

			transform.LookAt (m_ship.transform.position, m_ship.transform.up);

			yield return null;
		}

	}

	// Update is called once per frame
	void FixedUpdate ()
	{
		if (m_started) {

			m_timer += Time.deltaTime;

			if (m_timer > 1f) {
				m_countdown.text = m_timer.ToString ("#.00");
			}

			Vector3 targetPos = m_ship.transform.position + (m_ship.transform.forward * -6f) + (Vector3.up * 2f);

			transform.position = Vector3.SmoothDamp (transform.position, targetPos, ref m_velocity, m_smoothTime);

			transform.LookAt (m_ship.transform.position, m_ship.transform.up);
		}
	}
}
