﻿using UnityEngine;
using System.Collections;

namespace ZInput
{
	public interface ControlListener {

		void ButtonYPressed();
		void ButtonAPressed();
		void ButtonXPressed();
		void ButtonBPressed();
		void ButtonYReleased();
		void ButtonAReleased();
		void ButtonXReleased();
		void ButtonBReleased();
        void ButtonLPressed();
        void ButtonLReleased();
        void ButtonRPressed();
        void ButtonRReleased();
        void Axis(float x, float y);

		void AnyButtonReleased(CONTROLLER controller);
		void AnyButtonPressed(CONTROLLER controller);

	}
}
