﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UI;

public class GameBoy : MonoBehaviour
{
	private const string ANIM_GAMEBOYINTRO = "GameboyIntro"; 
	private const string ANIM_NONE = "None"; 

	[SerializeField] UIAnimator m_animator;
	[SerializeField] GameObject dot;

	public void PlayIntro()
	{
		
		m_animator.PlayAnimation (ANIM_GAMEBOYINTRO, () => GameObject.Find("GameBoyController").transform.localScale = Vector3.one * 2f);
		StartCoroutine ("DotOn");
	}

	public IEnumerator DotOn()
	{

		yield return new WaitForSeconds (4f);
		dot.active = true;

	}
}
