﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainSceneController : MonoBehaviour
{
	[SerializeField] CameraController m_camera;
	[SerializeField] Texture m_smashTexture;
	[SerializeField] MeshRenderer m_megaDrive;
	//[SerializeField] Material m_megaDrive;
	[SerializeField] Material m_n64;

	private bool m_gameboyClicked;

	void Awake()
	{
		SNESEvents.OutOfScreen += SNESEventsOutOfScreen;
	}

	void OnDestroy()
	{
		SNESEvents.OutOfScreen -= SNESEventsOutOfScreen;
	}

	void SNESEventsOutOfScreen ()
	{
		m_megaDrive.materials[0].SetTexture("_OcclusionTex", m_smashTexture);
		//m_megaDrive.SetTexture ("_OcclusionTex", m_smashTexture);
	}

	void N64EventsOutOfScreen ()
	{
		m_n64.SetTexture ("_OcclusionTex", m_smashTexture);
	}

	void Start()
	{
		SceneManager.LoadScene ("Gameboy_Game", LoadSceneMode.Additive);
	}

	// Update is called once per frame
	void Update ()
	{
		if (Input.GetKeyDown (KeyCode.R)) {
			
			if (CWSoundManager.Instance != null) {
				CWSoundManager.Instance.StopMusic ();
			}

			SceneManager.LoadScene ("MainScene", LoadSceneMode.Single);
		}

		if (m_gameboyClicked) {
			return;
		}

		if (Input.GetMouseButtonDown (0)) 
		{
			RaycastHit hit;

			Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);

			if (Physics.Raycast (ray, out hit, 100f)) 
			{
				if (hit.collider.gameObject.tag == "Gameboy") 
				{
					m_gameboyClicked = true;

					GameBoy gameboy = hit.collider.gameObject.GetComponent<GameBoy> ();
					gameboy.PlayIntro ();

					m_camera.LerpToIdentity ();
				}
			}
		}

	}
}
