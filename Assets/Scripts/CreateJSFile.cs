﻿#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;

public class CreateJSFile : EditorWindow
{
	private static string PATH = "Assets/Scripts/";
	private static string fileName = "";



	[MenuItem("Custom/JS File Maker")]
	static void ShowPopUp()
	{
		CreateJSFile window = (CreateJSFile)EditorWindow.GetWindow(typeof(CreateJSFile));
		GUIContent titleContent = new GUIContent ("JS File Maker");
		window.titleContent = titleContent;
		window.Show();
	}

	[MenuItem("Custom/Delete Player Prefs")]
	static void ClearPlayerPrefs()
	{

		PlayerPrefs.DeleteAll ();

	}

	void OnGUI()
	{
		EditorGUILayout.LabelField("File Name.", EditorStyles.wordWrappedLabel);
		GUILayout.Space(40);

		fileName = GUI.TextField(new Rect (10, 20, 200, 20), fileName);

		if (GUILayout.Button("Make File."))
		{
			MakeFile();
			this.Close ();
		}
	}

	static void  MakeFile()
	{
		if (string.IsNullOrEmpty(fileName))
		{
			return;
		}

		if (!File.Exists(PATH + fileName + ".js"))
		{
			string code = CreateClass();
			System.IO.File.WriteAllText(PATH + (fileName + ".js"), code.ToString());
UnityEditorInternal.InternalEditorUtility.OpenFileAtLineExternal(PATH + (fileName + ".js"), 1);
		}
		else
		{
			Debug.LogError("The file " + fileName + " already exist");
		}
		AssetDatabase.Refresh();
	}

	private static string CreateClass()
	{
		return @" 
#pragma strict

function Awake()
{
	//PlayerPrefs.DeleteAll();

	#if UNITY_IPHONE
	Application.targetFrameRate = 60;
	#endif

}

function Start () 
{

}

function Update () 
{
	if(Input.GetMouseButtonDown(0))
	{

	}
}

function OnTriggerEnter2D(other: Collider2D) 
{



}

function OnCollisionEnter2D(coll: Collision2D) 
{
    


}






//iTween.MoveTo(gameObject, iTween.Hash(""position"", Vector3(0,0,0), ""time"", 1.5, ""easetype"", iTween.EaseType.easeInOutSine));

";
	}
}

#endif