﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace ZInput
{
	public class InputBinder : MonoBehaviour {
		public ControllerManager controllerManager = new ControllerManager ();
		public bool[] isXBoxController = new bool[4];
		static InputBinder _instance;
		bool isKeyBoardActive = true;
		bool configurationMode = false;

		//public int[] playerOfController = new int[4] {-1,-1,-1,-1};
		//int playerOfKeyBoard = -1;

		ControlListener[] listenerOfController = new ControlListener[ControllerManager.NCONTROLLERS];
		ControlListener listenerOfKeyboard;

		List<ControllerInputAction> controllerActions;
		BUTTONBINDING[,] inputBindings = new BUTTONBINDING[ControllerManager.NCONTROLLERS, ControllerManager.NBUTTONS];

		public enum BUTTONBINDING
		{
			BUTTONNONE,
			BUTTONUP,
			BUTTONLEFT,
			BUTTONRIGHT,
			BUTTONDOWN,
			BUTTONL,
			BUTTONR,
			BUTTONL2,
			BUTTONR2,
			START,
			SELECT
		}

		void Awake(){
			if (_instance != null) {
				Destroy (this); //destroy the instance (singletone)
			} else {
				_instance = this;
				for (int i = 0; i < ControllerManager.NCONTROLLERS; ++i) 
					for (int j = 0; j < ControllerManager.NBUTTONS; ++j) 
						BindButton (i, j, BUTTONBINDING.BUTTONNONE);
			}
		}
		void Start () {

		}
		void Update () {

            if (Input.GetKeyDown(KeyCode.P))
            {
                Time.timeScale = 0.5f;
            }
            if (Input.GetKeyDown(KeyCode.O))
            {
                Time.timeScale = 1f;
            }

            if (configurationMode)
				controllerActions = controllerManager.ReadAllInput();
			else
				controllerActions = controllerManager.ReadRegisteredInput();


			//DoController
			//DoKeyBoard
			ExectuteKeyboardActions ();
            ExectuteControllerActions();

            if (Input.GetKeyUp(KeyCode.Escape))
            {
                Application.Quit();
            }
		}

		public static CONTROLLER getBindedController (ControlListener listener) {
			return _instance.getController (listener);
		}
		public CONTROLLER getController (ControlListener listener) {
			for (int i = 0; i < ControllerManager.NCONTROLLERS; ++i) {
				if (listenerOfController[i].Equals(listener)) 
					return (CONTROLLER)i;
			}
			if (listenerOfKeyboard.Equals(listener)) 
				return CONTROLLER.KEYBOARD;
			
			return CONTROLLER.NONE;
		}

		public static void setConfigurationMode (bool active) {
			_instance.configurationMode = active;
		}
		public static void BindControlToListener (CONTROLLER controller, ControlListener listener) {
			if (controller == CONTROLLER.KEYBOARD) _instance.BindKeyboard(listener);
			else _instance.BindControl((int)controller, listener);
		}

		static bool BindControlToListener (int controller, ControlListener listener) {
			return _instance.BindControl(controller, listener);
		}
		bool BindControl (int controller, ControlListener listener) {
            string[] joystickNames = Input.GetJoystickNames();

            if (controller >= joystickNames.Length)
            {
                return false;
            }

            listenerOfController [controller] = listener;
			controllerManager.registerInputSource ((CONTROLLER)controller);

			bool isXBOX = false;
            string joyName = joystickNames[controller];
            if (controller < joystickNames.Length)
			{
				isXBOX = joyName == "Microsoft Wireless 360 Controller" 
						|| joyName == "Controller (XBOX 360 For Windows)";
			}
			isXBoxController [controller] = isXBOX;

            print("Joy name "+controller+ " " + joystickNames[controller]);

            if (isXBOX) {
				print ("Binded xbox controller in " + controller);
				DoXBOXBindings (controller);
			} else {
				print ("Binded generic controller in "+controller);
				DoXBOXBindings (controller);
			}

            return true;
		}
		static void BindKeyboardToListener (ControlListener listener) {
			_instance.BindKeyboard(listener);
		}
		void BindKeyboard (ControlListener listener) {
			listenerOfKeyboard = listener;
			isKeyBoardActive = true;
		}


		void BindButton (int controller, int button, BUTTONBINDING gameAction) {
			inputBindings [controller, button] = gameAction;
		}

		void DoXBOXBindings (int controller) {
			BindButton (controller, 0, BUTTONBINDING.BUTTONDOWN);
			BindButton (controller, 1, BUTTONBINDING.BUTTONRIGHT);
			BindButton (controller, 2, BUTTONBINDING.BUTTONLEFT);
			BindButton (controller, 3, BUTTONBINDING.BUTTONUP);
			BindButton (controller, 4, BUTTONBINDING.BUTTONL);
			BindButton (controller, 5, BUTTONBINDING.BUTTONR);
			BindButton (controller, 6, BUTTONBINDING.SELECT);
			BindButton (controller, 7, BUTTONBINDING.START);
		}

		void ExectuteKeyboardActions()
		{
			if (isKeyBoardActive || configurationMode) 
			{
				ReadKeyBoardInput();
			}
		}

		void ExectuteControllerActions()
		{
			int size = controllerActions.Count;
			for (int i = 0; i < size; ++i) 
			{
				ControllerInputAction action =  controllerActions[i];
				ControlListener listener = listenerOfController[ action.controlID];
				if (listener == null) continue;

				ACTION actionType = action.action;

				//print ("action "+i+" - "+actionType);
		
				if (actionType == ACTION.BUTTONUP)
				{
					BUTTONBINDING bind= inputBindings[action.controlID, action.button];
					switch(bind)
					{
						case BUTTONBINDING.BUTTONDOWN:
							listener.ButtonAReleased();
							break;
						case BUTTONBINDING.BUTTONUP:
							listener.ButtonYReleased();
							break;
						case BUTTONBINDING.BUTTONLEFT:
							listener.ButtonXReleased();
							break;
						case BUTTONBINDING.BUTTONRIGHT:
							listener.ButtonBReleased();
							break;
                        case BUTTONBINDING.BUTTONR:
                            listener.ButtonRReleased();
                            break;
                        case BUTTONBINDING.BUTTONL:
                            listener.ButtonLReleased();
                            break;
                        case BUTTONBINDING.BUTTONNONE:
							//print("WARNING: (release) button not binded");
							break;
					}
					if (configurationMode) listener.AnyButtonReleased((CONTROLLER) action.controlID);
				}
				else if (actionType == ACTION.BUTTONDOWN)
				{
					BUTTONBINDING bind= inputBindings[action.controlID, action.button];
					switch(bind)
					{
						case BUTTONBINDING.BUTTONDOWN:
							listener.ButtonAPressed();
							break;
						case BUTTONBINDING.BUTTONUP:
							listener.ButtonYPressed();
							break;
						case BUTTONBINDING.BUTTONLEFT:
							listener.ButtonXPressed();
							break;
						case BUTTONBINDING.BUTTONRIGHT:
							listener.ButtonBPressed();
							break;
                        case BUTTONBINDING.BUTTONR:
                            listener.ButtonRPressed();
                            break;
                        case BUTTONBINDING.BUTTONL:
                            listener.ButtonLPressed();
                            break;
                        case BUTTONBINDING.BUTTONNONE:
							print("WARNING: (press) button not binded");
							break;
					}
					if (configurationMode) listener.AnyButtonPressed((CONTROLLER) action.controlID);
				}
				else if (actionType == ACTION.AXIS 
                    && (listener != listenerOfKeyboard || (listener == listenerOfKeyboard && !anyMoveKey)))
				{
					listener.Axis (action.axisX,action.axisY);
                   
                }
                anyMoveKey = false;
            }

		}

		bool anyKey = false;
        bool anyMoveKey = false;
        public void ReadKeyBoardInput()
		{
			//This should be in a keyboard manager, but will make things more complicated than neccesary
			ControlListener listener = listenerOfKeyboard;
			if (listener == null) return;
			
			Vector2 axis = new Vector2(0,0);
			if (Input.GetKey(KeyCode.UpArrow) || Input.GetKey (KeyCode.W))
			{
				axis += new Vector2(0,1);
                anyMoveKey = true;
            }
			if (Input.GetKey(KeyCode.DownArrow) || Input.GetKey (KeyCode.S))
			{
				axis += new Vector2(0,-1);
                anyMoveKey = true;
            }
			if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey (KeyCode.A))
			{
				axis += new Vector2(-1,0);
                anyMoveKey = true;
            }
			if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey (KeyCode.D))
			{
				axis += new Vector2(1,0);
                anyMoveKey = true;
            }
			listener.Axis (axis.x,axis.y);

			//if (Input.GetKeyDown(KeyCode.LeftArrow))
			//{
			//	listener.ButtonXPressed();
			//}
			//else if (Input.GetKeyUp(KeyCode.LeftArrow))
			//{
			//	listener.ButtonXReleased();
			//}

			/*if (Input.GetKeyDown(KeyCode.RightArrow))
			{
				listener.ButtonBPressed();
			}
			else if (Input.GetKeyUp(KeyCode.RightArrow))
			{
				listener.ButtonBReleased();
			}*/

			/*if (Input.GetKeyDown(KeyCode.UpArrow))
			{
				listener.ButtonYPressed();
			}
			else if (Input.GetKeyUp(KeyCode.UpArrow))
			{
				listener.ButtonYReleased();
			}*/

			if (Input.GetMouseButtonDown(0))
			{
				listener.ButtonAPressed();
			}
			else if (Input.GetMouseButtonUp(0))
			{
				listener.ButtonAReleased();
			}

            if (Input.GetKeyDown(KeyCode.Space))
            {
                listener.ButtonAPressed();
            }
            else if (Input.GetKeyDown(KeyCode.Space))
            {
                listener.ButtonAReleased();
            }

            /*if (Input.GetKeyDown(KeyCode.E))
            {
                listener.ButtonRPressed();
            }
            else if (Input.GetKeyUp(KeyCode.E))
            {
                listener.ButtonRReleased();
            }

            if (Input.GetKeyDown(KeyCode.Q))
            {
                listener.ButtonLPressed();
            }
            else if (Input.GetKeyUp(KeyCode.Q))
            {
                listener.ButtonLReleased();
            }*/

            if (configurationMode) {
				if (Input.anyKey)
				{
					bool isKeyArrow = Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.LeftArrow);
					if (Input.inputString.Length == 0 && !isKeyArrow) return;

					if (anyKey == false)
					{
						listener.AnyButtonPressed(CONTROLLER.KEYBOARD);
						anyKey = true;
					}
				}
				else{
					if (anyKey == true)
					{
						listener.AnyButtonReleased(CONTROLLER.KEYBOARD);
						anyKey = false;
					}
				}
			}
		}


	}
}

