﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Outro : MonoBehaviour {

	[SerializeField] Image m_fadeToBlack;
	[SerializeField] GameObject m_thanks;
	[SerializeField] Text m_thanksText;

	// Use this for initialization
	void Start () {
		StartCoroutine (FadeToBlack ());
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	IEnumerator FadeToBlack()
	{
		yield return new WaitForSeconds (3.5f);

		float time = 0f;

		Color startColour = m_fadeToBlack.color;

		while (time < 5f) {
			
			time += Time.deltaTime;

			m_fadeToBlack.color = Color.Lerp (startColour, new Color (0, 0, 0, 1f), time / 5f);

			yield return null;
		}

		m_fadeToBlack.color = new Color (0, 0, 0, 1f);


		m_thanks.SetActive(true);

		time = 0f;

		while (time < 5f) {

			time += Time.deltaTime;

			m_thanksText.color = Color.Lerp (startColour, new Color (1, 1, 1, 1f), time / 5f);

			yield return null;
		}

		yield return new WaitForSeconds (5f);

		if (CWSoundManager.Instance != null) {
			CWSoundManager.Instance.StopMusic ();
		}

		SceneManager.LoadScene ("MainScene", LoadSceneMode.Single);
	}
}
