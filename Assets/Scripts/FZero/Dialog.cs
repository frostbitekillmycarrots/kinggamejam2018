﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dialog : MonoBehaviour {

	[SerializeField] FZeroCam m_cam;
	// Use this for initialization
	void Start () {
		//Time.timeScale = 0f;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButtonDown (0)) {
			gameObject.SetActive (false);
			m_cam.StartIntro ();
			Time.timeScale = 1f;
            CWSoundManager.Instance.PlayMusic(GameSounds.Instance.m_musicCar);
        }
	}
}
