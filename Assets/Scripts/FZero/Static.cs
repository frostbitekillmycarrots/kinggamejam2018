﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Static : MonoBehaviour {

	[SerializeField] Image m_image;
	[SerializeField] Sprite[] m_static;
	[SerializeField] GameObject m_dialog;

	// Use this for initialization
	void Start () {
		StartCoroutine (SpriteSwap ());
	}

	IEnumerator SpriteSwap()
	{
		float timer = 0f;

		int count = 0;

		while (timer < 4f) {
		
			m_image.sprite = m_static [count % m_static.Length];

			timer += Time.deltaTime;

			++count;

			yield return null;
			yield return null;
					
		}

		m_dialog.SetActive (true);
		gameObject.SetActive (false);
	}
}
