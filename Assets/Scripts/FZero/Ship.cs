﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ZInput;

public class Ship : MonoBehaviour, ZInput.ControlListener
{
	private const int LAPS = 3;

	[SerializeField] Rigidbody m_body;
	[SerializeField] Vector3 m_upForce;
	[SerializeField] float m_hoverHeight;
	[SerializeField] FZeroCam m_cam;
	[SerializeField] GameObject[] m_lapNum;

	RaycastHit m_hit;
	private float m_yaw = 0f;
	private float m_pitch = 0f;
	private float m_roll = 0f;

	private int m_lap;

	private Vector3 m_statPos;

	private bool m_go;

//	private float m_turnRoll = 0f;

	// Use this for initialization
	void Start ()
	{
		InputBinder.BindControlToListener (CONTROLLER.KEYBOARD, this);
	}

	public void SetGo()
	{
		m_statPos = transform.position;
		m_go = true;
	}

	IEnumerator ResetShip()
	{
		yield return new WaitForSeconds (2f);

		transform.position = m_statPos;
	}

	void FixedUpdate ()
	{
		if (Physics.Raycast (new Ray (transform.position, Vector3.down), out m_hit, 6f, LayerMask.GetMask(new string[1] {"Track"}))) {
			Vector3 upForce = m_upForce * (m_hoverHeight - m_hit.distance);

			m_body.velocity += upForce;

			StopAllCoroutines ();

//			Vector3 pos = transform.transform.position;
//			pos.y = (m_hit.point + (m_hit.normal * m_hoverHeight)).y;
//			transform.position = pos;

		} else {
			StartCoroutine (ResetShip ());
		}

		Vector3 forward = Vector3.Cross (transform.right, m_hit.normal);
		Vector3 right = Vector3.Cross (forward, m_hit.normal);

		Debug.DrawRay (transform.position, forward * 10f);
		Debug.DrawRay (transform.position, right * 10f);
		Debug.DrawRay (transform.position, m_hit.normal * 10f);
		Debug.DrawRay (transform.position, transform.forward * 10f);

		if (m_go) {
			if (Input.GetKey (KeyCode.LeftArrow) || Input.GetKey (KeyCode.A)) {
				m_yaw -= 5f;

				if (m_roll < 20f) {
					m_roll += 1f;
				}

				Vector3 eula = new Vector3 (transform.localEulerAngles.x, m_yaw, transform.localEulerAngles.z);

				transform.localEulerAngles = eula;

			} else if (Input.GetKey (KeyCode.RightArrow) || Input.GetKey (KeyCode.D)) {
				m_yaw += 5f;

				if (m_roll > -20f) {
					m_roll -= 1f;
				}

				Vector3 eula = new Vector3 (transform.localEulerAngles.x, m_yaw, transform.localEulerAngles.z);

				transform.localEulerAngles = eula;
			} else {
				//m_yaw = 0f;//Mathf.Lerp (m_yaw, 0f, 0.1f);
			}
		}

		forward = Quaternion.AngleAxis (m_yaw, m_hit.normal) * forward;

		if (Input.GetMouseButton(0) && m_go) 
		{
			m_body.velocity += transform.forward * 2f;
		}

		//Quaternion rot = Quaternion.LookRotation (forward, m_hit.normal);

		//transform.rotation = Quaternion.Lerp(transform.rotation, rot, 0.5f);
		//transform.rotation = rot;
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.tag == "Finish") {

			m_lapNum [m_lap].SetActive (false);
			++m_lap;

			if (m_lap >= LAPS) {
				m_cam.StopTimer ();
			} else {
				m_lapNum [m_lap].SetActive (true);
			}
		}
	}

	#region ControlListener implementation

	public void ButtonYPressed ()
	{
	}

	public void ButtonAPressed ()
	{
	}

	public void ButtonXPressed ()
	{
	}

	public void ButtonBPressed ()
	{
	}

	public void ButtonYReleased ()
	{
	}

	public void ButtonAReleased ()
	{
	}

	public void ButtonXReleased ()
	{
	}

	public void ButtonBReleased ()
	{
	}

	public void ButtonLPressed ()
	{
	}

	public void ButtonLReleased ()
	{
	}

	public void ButtonRPressed ()
	{
	}

	public void ButtonRReleased ()
	{
	}

	public void Axis (float x, float y)
	{
	}

	public void AnyButtonReleased (ZInput.CONTROLLER controller)
	{
	}

	public void AnyButtonPressed (ZInput.CONTROLLER controller)
	{
	}

	#endregion
}
