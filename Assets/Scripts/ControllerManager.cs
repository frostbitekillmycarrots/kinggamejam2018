﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace ZInput
{
	public struct ControllerInputAction
	{
		public ControllerInputAction (int _controller, float _axisX, float _axisY)
		{
			controlID = _controller;
			action = ACTION.AXIS;
			axisX = _axisX;
			axisY = _axisY;
			button = -1;
		}
		
		public ControllerInputAction (int _controller, int _button, ACTION _action)
		{
			controlID = _controller;
			action = _action;
			button = _button;
			axisX = 0;
			axisY = 0;
		}
		
		public int controlID;
		public ACTION action;
		public float axisX;
		public float axisY;
		public int button;
	}
	public enum CONTROLLER
	{
		NONE = -1,
		CONTROLLER1,
		CONTROLLER2,
		CONTROLLER3,
		CONTROLLER4,
		KEYBOARD,
		SIZE
	}
	public enum ACTION
	{
		AXIS,
		BUTTONUP,
		BUTTONDOWN,
	}

	public class ControllerManager {

		public const int NCONTROLLERS = 4;
		public const int NBUTTONS = 10;

		//public bool[] checkController= new bool[4]{true, true, true, true};
		private List<ControllerInputAction> actionList = new List<ControllerInputAction>();
		private List<CONTROLLER> registeredInput = new List<CONTROLLER>();

		public ControllerManager()
		{
		}

		public void registerInputSource(CONTROLLER controller)
		{
			registeredInput.Add (controller);
		}

		public void reset()
		{
			registeredInput.Clear ();
		}


		string[,] mapname= new string[NCONTROLLERS,NBUTTONS]{
			{	"joystick 1 button 0",
			 	"joystick 1 button 1",
			 	"joystick 1 button 2",
			 	"joystick 1 button 3",
			 	"joystick 1 button 4",
			 	"joystick 1 button 5",
				"joystick 1 button 6",
				"joystick 1 button 7",
				"joystick 1 button 7",
				"joystick 1 button 9"
			},
			{	"joystick 2 button 0",
				"joystick 2 button 1",
				"joystick 2 button 2",
				"joystick 2 button 3",
				"joystick 2 button 4",
				"joystick 2 button 5",
				"joystick 2 button 6",
				"joystick 2 button 7",
				"joystick 2 button 7",
				"joystick 2 button 9"
			},
			{	"joystick 3 button 0",
				"joystick 3 button 1",
				"joystick 3 button 2",
				"joystick 3 button 3",
				"joystick 3 button 4",
				"joystick 3 button 5",
				"joystick 3 button 6",
				"joystick 3 button 7",
				"joystick 3 button 7",
				"joystick 3 button 9"
			},
			{	"joystick 4 button 0",
				"joystick 4 button 1",
				"joystick 4 button 2",
				"joystick 4 button 3",
				"joystick 4 button 4",
				"joystick 4 button 5",
				"joystick 4 button 6",
				"joystick 4 button 7",
				"joystick 4 button 7",
				"joystick 4 button 9"
			}
		};

		string[,] joymapname= new string[NCONTROLLERS,2]{
			{	"Horizontal",
				"Vertical"
			},
			{	"HorizontalJ2",
				"VerticalJ2"
			},
			{	"HorizontalJ3",
				"VerticalJ3"
			},
			{	"HorizontalJ4",
				"VerticalJ4"
			}
		};

		public List<ControllerInputAction> ReadRegisteredInput()
		{
			//This one will read from a list of mapped input by the player
			//It will be preset for Xbox controllers
			actionList.Clear (); //TODO: change this for a pool of actions
			foreach (CONTROLLER controller in registeredInput)
			{
				checkForActions((int)controller);
			}
			return actionList;
		}

		public List<ControllerInputAction> ReadAllInput()
		{
			actionList.Clear (); //TODO: change this for a pool of actions
			for (int controller = 0; controller < NCONTROLLERS; ++controller)
			{
				checkForActions(controller);
			}
			return actionList;
		}

		public void checkForActions(int controller)
		{
			for (int button = 0; button < NBUTTONS; ++button) {
				string map = mapname[controller,button];
				if (Input.GetKeyDown (map))
				{
					actionList.Add(new ControllerInputAction(controller, button, ACTION.BUTTONDOWN));//TODO: change this for a pool of actions
				}
				else if (Input.GetKeyUp (map))
				{
					actionList.Add(new ControllerInputAction(controller, button, ACTION.BUTTONUP));//TODO: change this for a pool of actions
				}
			}

			float axisX = Input.GetAxis (joymapname [controller, 0]);
			float axisY = Input.GetAxis (joymapname [controller, 1]);

			actionList.Add(new ControllerInputAction(controller, axisX, axisY));
		}

	}//END OF CLASS
}//END OF NAMESPACE
