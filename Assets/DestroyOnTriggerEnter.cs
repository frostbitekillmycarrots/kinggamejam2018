﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyOnTriggerEnter : MonoBehaviour {

    void OnTriggerEnter2D(Collider2D other)
    {
        gameObject.SetActive(false);
    }
}
