﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SplashZOmbi : MonoBehaviour {

    [SerializeField] SNESMainCharacter m_main;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButtonDown(0) || Input.GetKeyDown(KeyCode.Space))
        {
            m_main.Init();
            gameObject.SetActive(false);
            //CWSoundManager.Instance.PlayEffect(GameSounds.Instance.UIClick);
            CWSoundManager.Instance.PlayMusic(GameSounds.Instance.m_musicMain);
        }
	}
}
